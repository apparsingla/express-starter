docker stop dan-kitchen-sink-service
docker rm dan-kitchen-sink-service
docker run -p 8481:8481 -it -d --env-file .env --name=dan-kitchen-sink-service dan-kitchen-sink-service:latest
