import logger from '../config/winston';
import { IApplication } from '../entities';
import { Nullable } from '../libs/Nullable';
import ApplicationRepository from '../repositories/business/application/ApplicationRepository';

export default class ApplicationController {
  public static getInstance() {
    if (!ApplicationController.instance) {
      ApplicationController.instance = new ApplicationController();
    }

    return ApplicationController.instance;
  }
  private static instance: ApplicationController;

  /* tslint:disable:variable-name */
  private _applicationRepository: ApplicationRepository;

  /* tslint:disable:no-null-keyword */
  private constructor(applicationRepository: Nullable<ApplicationRepository> = null) {
    this._applicationRepository = applicationRepository ? applicationRepository : new ApplicationRepository();
  }

  /**
   * Create new application
   * @property {string} name - The technical (unique) name of the application.
   * @property {string} caption - The displayable name of the application.
   * @returns {IApplication}
   */
  public async create(name: string, caption: string): Promise<Nullable<IApplication>> {
    try {
      logger.debug('ApplicationController - Create');

      const validCaption = caption ? caption : name;

      return ApplicationController.getInstance()._applicationRepository.create({
        caption: validCaption,
        name,
      });
    } catch (e) {
      logger.error('ApplicationController - Create exception -->>', e);
      throw (e);
    }
  }

  /**
   * Get applications list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {IApplication[]}
   */
  public async list(limit: number, skip: number): Promise<IApplication[]> {
    try {
      logger.debug('ApplicationController - List');
      return ApplicationController.getInstance()._applicationRepository.list({ limit, skip });
    } catch (e) {
      logger.error('ApplicationController - List exception -->>', e);
      throw (e);
    }
  }

  /**
   * Get Application.
   * @property {string} id - Application unique identifier
   * @returns {IApplication}
   */
  public async get(id: string): Promise<Nullable<IApplication>> {
    try {
      logger.debug('ApplicationController - Get');
      return ApplicationController.getInstance()._applicationRepository.get({ id });
    } catch (e) {
      logger.error('ApplicationController - Get exception -->>', e);
      throw (e);
    }
  }

  /**
   * Update existing application.
   * @property {string} id - Unique identifier Id of an application
   * @property {string} name - Updated name for the exixting application
   * @property {string} caption - Updated caption for the existing application
   * @returns {IApplication}
   */
  public async update(id: string, name?: string, caption?: string): Promise<Nullable<IApplication>> {
    try {
      logger.debug('ApplicationController - Update');
      const options: any = { originalId: id };
      if (name) { options.name = name; }
      if (caption) { options.caption = caption; }
      return ApplicationController.getInstance()._applicationRepository.update(options);
    } catch (e) {
      logger.error('ApplicationController - Update exception -->>', e);
      throw (e);
    }
  }
}
