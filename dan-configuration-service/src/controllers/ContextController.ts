import logger from '../config/winston';
import { IContext } from '../entities';
import { BadIdError } from '../libs/errors/data';
import { Nullable } from '../libs/Nullable';
import ApplicationRepository from '../repositories/business/application/ApplicationRepository';
import ContextRepository from '../repositories/business/context/ContextRepository';
import EnvironmentRepository from '../repositories/business/environment/EnvironmentRepository';
export default class ContextController {
  public static getInstance() {
    if (!ContextController.instance) {
      ContextController.instance = new ContextController();
    }

    return ContextController.instance;
  }
  private static instance: ContextController;

  /* tslint:disable:variable-name */
  private _contextRepository: ContextRepository;
  private applicationRepository: ApplicationRepository;
  private environmentRepository: EnvironmentRepository;

  /* tslint:disable:no-null-keyword */
  private constructor(contextRepository: Nullable<ContextRepository> = null) {
    this._contextRepository = contextRepository ? contextRepository : new ContextRepository();
    this.applicationRepository = new ApplicationRepository();
    this.environmentRepository = new EnvironmentRepository();
  }

  /**
   * Create new context
   * @property {string} url - The base url for the application instance.
   * @property {string} application - The application referred by the Context.
   * @property {string} environment - The environment referred by the Context.
   * @returns {IContext}
   */
  public async create(url: string, application: string, environment: string): Promise<Nullable<IContext>> {
    try {
      logger.debug('ContextController - Create');

      if (typeof application && application) {
        await this.checkApplicationExistence(application);
      }
      if (typeof environment && environment) {
        await this.checkEnvironmentExistence(environment);
      }

      return ContextController.getInstance()._contextRepository.create({
        application,
        environment,
        url,
      });
    } catch (e) {
      logger.error('ContextController - Create exception --> ', e);
      throw (e);
    }
  }

  /**
   * Get contexts list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {IContext[]}
   */
  public async list(limit: number, skip: number): Promise<IContext[]> {
    try {
      logger.debug('ContextController - List');

      return ContextController.getInstance()._contextRepository.list({ limit, skip });
    } catch (e) {
      logger.error('ContextController - List exception --> ', e);
      throw (e);

    }
  }

  /**
   * Get Context.
   * @property {string} id - Context unique identifier
   * @returns {IContext}
   */
  public async get(id: string): Promise<Nullable<IContext>> {
    try {
      logger.debug('ContextController - Get');

      return ContextController.getInstance()._contextRepository.get({ id });
    } catch (e) {
      logger.error('ContextController - Get exception --> ', e);
      throw (e);

    }
  }

  /**
   * Update existing context.
   * @property {string} id - the unique identifier for the context to be updated.
   * @property {string} url - The base url for the application instance.
   * @property {string} application - The application referred by the Context.
   * @property {string} environment - The environment referred by the Context.
   * @returns {IContext}
   */
  public async update(
    id: string,
    url?: string,
    application?: string,
    environment?: string): Promise<Nullable<IContext>> {
    try {
      logger.debug('ContextController - Update');

      const options: any = { originalId: id };
      if (url) { options.url = url; }
      if (typeof application && application) {
        await this.checkApplicationExistence(application);
        options.application = application;
      }
      if (typeof environment && environment) {
        await this.checkEnvironmentExistence(environment);
        options.environment = environment;
      }
      return ContextController.getInstance()._contextRepository.update(options);
    } catch (e) {
      logger.error('ContextController - Update exception --> ', e);
      throw (e);
    }
  }

  /**
   * Check the existence for Application instance
   * @param {String} id Application identifier
   */
  private async checkApplicationExistence(id: string) {
    const application = await this.applicationRepository.get({ id });
    logger.info('checkApplicationExistence:: application:::', application);
    if (!application) {
      throw new BadIdError(
        `Application id [${id}] does not exist`,
        'id',
        id,
      );
    }
  }

  /**
   * Check the existence for Environment instance
   * @param {String} id Environment identifier
   */
  private async checkEnvironmentExistence(id: string) {
    const environment = await this.environmentRepository.get({ id });
    logger.info('checkEnvironmentExistence:: environment:::', environment);
    if (!environment) {
      throw new BadIdError(
        `Environment id [${id}] does not exist`,
        'id',
        id,
      );
    }
  }
}
