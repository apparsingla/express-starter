import logger from '../config/winston';
import { IUser } from '../entities';
import { Nullable } from '../libs/Nullable';
import UserRepository from '../repositories/business/user/UserRepository';

export default class UserController {
  public static getInstance(): UserController {
    if (!UserController.instance) {
      UserController.instance = new UserController();
    }
    return UserController.instance;
  }
  private static instance: UserController;
  /* tslint:disable:variable-name */
  private _userRepository: UserRepository;

  /* tslint:disable:no-null-keyword */
  private constructor(userRepository: Nullable<UserRepository> = null) {
    this._userRepository = userRepository ? userRepository : new UserRepository();
  }
/**
 * Create a new user
 * @param {string} name - Name of the user
 * @param {string} email - email of the user
 * @param {string} username - username of the user
 * @param {string} password - password of the user
 * @returns {IUser}
 */
  public create(name: string, email: string, username: string, password: string): Promise<Nullable<IUser>> {
    try {
      return UserController.instance._userRepository.create({
        email,
        name,
        password,
        username,
      });
    }
    catch (e) {
      logger.error('UserController - Create exception -->>', e);
      throw (e);
    }
  }
  /**
   * Delete User.
   * @property {string} id - User unique identifier
   * @returns {IUser}
   */
  public async delete(id: string): Promise<Nullable<IUser>> {
    try {
      logger.debug('UserController - Delete');
      const options: any = { id };
      return UserController.getInstance()._userRepository.delete(options);
    } catch (e) {
      logger.error('UserController - Delete exception -->>', e);
      throw (e);
    }
  }
  /**
   * Get User.
   * @property {string} id - User unique identifier
   * @returns {IUser}
   */
  public async get(id: string): Promise<Nullable<IUser>> {
    try {
      logger.debug('UserController - Get');
      return UserController.getInstance()._userRepository.get({ id });
    } catch (e) {
      logger.error('UserController - Get exception -->>', e);
      throw (e);
    }
  }
  /**
   * Get users list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {IUser[]}
   */
  public async list(limit: number, skip: number): Promise<IUser[]> {
    try {
      logger.debug('UserController - List');
      return UserController.getInstance()._userRepository.list({ limit, skip });
    } catch (e) {
      logger.error('UserController - List exception -->>', e);
      throw (e);
    }
  }
  /**
   * Update existing user details.
   * @property {string} id - Unique identifier Id of an user
   * @property {string} name - Updated name for the existing user
   * @returns {IUser}
   */
  public async update(id: string, name?: string): Promise<Nullable<IUser>> {
    try {
      logger.debug('UserController - Update');
      const options: any = { originalId: id };
      if (name) { options.name = name; }
      return UserController.getInstance()._userRepository.update(options);
    } catch (e) {
      logger.error('UserController - Update exception -->>', e);
      throw (e);
    }
  }
}
