import logger from '../config/winston';
import { IConfiguration } from '../entities';
import APIError from '../libs/errors/APIError';
import { BadIdError } from '../libs/errors/data';
import DuplicateError from '../libs/errors/data/DuplicateError';
import { Nullable } from '../libs/Nullable';
import ApplicationRepository from '../repositories/business/application/ApplicationRepository';
import ConfigurationRepository from '../repositories/business/configuration/ConfigurationRepository';
import ContextRepository from '../repositories/business/context/ContextRepository';
import EnvironmentRepository from '../repositories/business/environment/EnvironmentRepository';
import FieldRepository from '../repositories/business/field/FieldRepository';
import SettingRepository from '../repositories/business/setting/SettingRepository';

interface INormalizedEntry {
  key: string;
  value: any;
}

interface IContextQuery {
  application?: string;
  environment?: string;
}

interface IConfigurationQuery {
  limit: number;
  skip: number;
  context?: any;
}

export default class ConfigurationController {

  public static getInstance() {
    if (!ConfigurationController.instance) {
      ConfigurationController.instance = new ConfigurationController();
    }

    return ConfigurationController.instance;
  }

  private static instance: ConfigurationController;

  /* tslint:disable:variable-name */
  private _configurationRepository: ConfigurationRepository;
  private applicationRepository: ApplicationRepository;
  private contextRepository: ContextRepository;
  private environmentRepository: EnvironmentRepository;
  /* tslint:disable:no-null-keyword */
  private constructor(configurationRepository: Nullable<ConfigurationRepository> = null) {
    this._configurationRepository = configurationRepository ? configurationRepository : new ConfigurationRepository();
    this.applicationRepository = new ApplicationRepository();
    this.contextRepository = new ContextRepository();
    this.environmentRepository = new EnvironmentRepository();
  }

  /**
   * Create new configuration
   * @property {string} context - Reference to the context the configuration applies to.
   * @property {string[]} fields - References to the fields being part of the configuration.
   */

  public async create(context: string, configurationBody: any): Promise<Nullable<IConfiguration>> {
    try {
      logger.debug('ConfigurationController - Create');

      const contextRepository = new ContextRepository();
      const contextDetails = await contextRepository.get({id: context});

      if (!contextDetails) {
        throw new BadIdError(
          `Context id [${context}] does not exist`,
          'context',
          context,
        );
      }

      const configuration = await ConfigurationController.getInstance()._configurationRepository.
        getConfigByQuery({ context, deletedAt: undefined });

      if (configuration) {
        throw new DuplicateError(
          `Configuration for context ${context} already exists. Cannot be duplicated`,
          'context',
          { context: configuration.context },
        );
      }

      const normalizedBody = this.normalizeJSON(configurationBody);

      const fieldIds: string[] = [];

      // for(let i = 0; i<normalizedBody.length; i++){

      //   const option: INormalizedEntry = normalizedBody[i];
      //   const setting = await this.createSetting(option);
      //   const field = await this.createField(option, setting);

      //   fieldIds.push(field.id);

      // }

      for (const option of normalizedBody) {
        const setting = await this.createSetting(option);
        const field = await this.createField(option, setting);

        fieldIds.push(field.id);
      }

      // 3.Creating Configuration
      const config = await ConfigurationController.getInstance()._configurationRepository.create({
        context,
        fields: fieldIds,
      });

      return this.get(config.id);
      // return null;
    } catch (e) {
      logger.error('ConfigurationController - Create exception --> ', e);
      throw (e);
    }
  }

  /**
   * Get configurations list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {IConfiguration[]}
   */
  public async list(limit: number, skip: number, application?: string, environment?: string):
    Promise<IConfiguration[]> {
    try {
      const contextQuery: IContextQuery = {};
      let isContextExist: boolean = false;
      const configQuery: IConfigurationQuery = {
        limit,
        skip,
      };
      if (typeof application && application) {
        await this.checkApplicationExistence(application);
        contextQuery.application = application;
        isContextExist = true;
      }
      if (typeof environment && environment) {
        await this.checkEnvironmentExistence(environment);
        contextQuery.environment = environment;
        isContextExist = true;
      }
      if (isContextExist) {
        let context = await this.contextRepository.list(contextQuery);
        configQuery.context = { $in: [] };
        if (context && context.length > 0) {
          context = this.getOriginalIds(context);
          configQuery.context = { $in: context };
        }
      }
      logger.debug('ConfigurationController - List');
      return ConfigurationController.getInstance()
      ._configurationRepository.list(configQuery);
    } catch (e) {
      logger.error('ConfigurationController - List exception --> ', e);
      throw (e);
    }
  }

  /**
   * Get Configuration.
   * @property {string} id - Configuration unique identifier
   * @returns {IConfiguration}
   */
  public async get(id: string): Promise<Nullable<IConfiguration>> {
    try {

      logger.debug('ConfigurationController - Get');
      return ConfigurationController.getInstance()._configurationRepository.get({ id });
    } catch (e) {
      logger.error('ConfigurationController - Get exception --> ', e);
      throw (e);
    }
  }

  /**
   * Update existing configuration.
   * @property {string} id - Unique identifier Id of an configuration
   * @property {string[]} configurationBody - The configuration as it should look like, in JSON format
   * @returns {IConfiguration}
   */
  public async update(id: string, configurationBody: any): Promise<Nullable<IConfiguration>> {
    logger.debug('ConfigurationController - Update');

    const oldConfig: any = await this.get(id);
    const allOldFields: any[] = oldConfig.fieldsDetails;
    const newNormalized: INormalizedEntry[] = this.normalizeJSON(configurationBody);
    let fieldIds: string[] = allOldFields.map((field) => field.originalId);
    logger.debug('FIELDS IDS::::', fieldIds);

    // 1.Create/Update  Fields/Settings
    for (const nextEntry of newNormalized) {

      const matchingOldFields = allOldFields.filter((field) => field.path === nextEntry.key);
      const matchingOldField = matchingOldFields && matchingOldFields.length > 0 ? matchingOldFields[0] : null;

      if (matchingOldField) {
        if (matchingOldField.settingDetails.value !== nextEntry.value) {
          // 1.1.Field already exists and value is different: updating Setting

          logger.debug('Found a Setting that changed: updating...');
          await this.updateSetting(matchingOldField.settingDetails.originalId, nextEntry);
        }

      } else {  // 1.2.Field/Setting do not exist:creating

        logger.debug(`Found a new Field [${nextEntry.key}]: creating...`);
        const setting = await this.createSetting(nextEntry);
        const field = await this.createField(nextEntry, setting);
        fieldIds.push(field.originalId);
      }
    }

    // 2. Delete Fields/Settings
    for (const nextOldField of oldConfig.fieldsDetails) {

      const path = this.stringPathToArray(nextOldField.path);
      const matchingNewEntry = this.resolveJSONPath(path, configurationBody);

      if (matchingNewEntry == null) {
        // ATTENTION: check for this to be null,
        // don't just use !matchingNewEntry (e.g.: value==false ---> the field exists, but the test will fail)

        logger.debug(`Found a Field was removed: deleting [${path}]...`);
        await this.deleteSetting(nextOldField.settingDetails.originalId);
        await this.deleteField(nextOldField.originalId);
        fieldIds = fieldIds.filter((fieldId) => fieldId !== nextOldField.originalId);
      }
    }

    // 3. Updating Config
    await this._configurationRepository.update({ originalId: id, fields: fieldIds });

    return this.get(id);
  }

  private getType(something: any) {
    return (something instanceof Array) ? 'array' : typeof something;
  }

  private normalizeJSON(denormalized: any, prefix?: string) {

    let entries = [];

    Object.keys(denormalized).forEach((key) => {

      const value = denormalized[key];
      const type = this.getType(value);

      if (type === 'array') {

        value.forEach((obj, i) => {
          const objType = this.getType(obj);
          const isObj = objType === 'array' || objType === 'object';
          if (isObj) {
            const fullyQualifiedKey = prefix ? `${prefix}.${key}[${i}]` : `${key}[${i}]`;
            entries = entries.concat(this.normalizeJSON(obj, fullyQualifiedKey));
          } else {
            const fullyQualifiedKey = prefix ? `${prefix}.${key}[${i}]` : `${key}[${i}]`;
            entries.push({ key: fullyQualifiedKey, value: value[i] });
          }

        });
      }
      else if (type === 'object') {
        const fullyQualifiedKey = prefix ? `${prefix}.${key}` : key;
        entries = entries.concat(this.normalizeJSON(value, fullyQualifiedKey));
      } else {

        const fullyQualifiedKey = prefix ? `${prefix}.${key}` : key;
        entries.push({ key: fullyQualifiedKey, value });
      }
    });

    return entries;
  }

  private stringPathToArray(path: string): string[] {
    const parsablePath = path.replace('[', '.').replace(']', '');
    return parsablePath.split('.');
  }

  private resolveJSONPath(path: string[], object: any) {

    try {
      logger.debug('RESOLVE JSON PATH::::', path, object);
      let value = object;
      // for (let i = 0; i < path.length; i++) {
      //   loger.debug('PATH::::', path[i]);
      //   value = value[path[i]];
      //   Loger.debug('PATH VALUE:::', value);
      // }
      for (const pathValue of path) {
        logger.debug('PATH::::', pathValue);
        value = value[pathValue];
        logger.debug('PATH VALUE::::', value);
      }

      return value;
    } catch (e) {      // path does not exist in object
      return null;
    }

  }

  private async updateSetting(id: string, rawData: INormalizedEntry) {

    const settingRepository = new SettingRepository();

    const setting = await settingRepository.update({
      name: rawData.key.split('.')[rawData.key.split('.').length - 1],
      originalId: id,
      type: this.getType(rawData.value),
      value: rawData.value,
    });

    return setting;
  }

  private async deleteSetting(id: string) {

    const settingRepository = new SettingRepository();
    settingRepository.delete({ id });

  }

  private async createSetting(rawData: INormalizedEntry) {

    const settingRepository = new SettingRepository();

    const setting = await settingRepository.create({
      name: rawData.key.split('.')[rawData.key.split('.').length - 1],
      type: this.getType(rawData.value),
      value: rawData.value,
    });

    return setting;
  }

  private async createField(rawData: INormalizedEntry, setting: any) {

    const fieldRepository = new FieldRepository();

    const field = await fieldRepository.create({
      internalName: setting.name,
      path: rawData.key,
      setting: setting.id,
    });

    return field;
  }

  private async deleteField(id: string) {

    const fieldRepository = new FieldRepository();
    fieldRepository.delete({ id });

  }

  /**
   * Check the existence for Application instance
   * @param {String} id Application identifier
   */
  private async checkApplicationExistence(id: string) {
    const application = await this.applicationRepository.get({ id });
    logger.info('checkApplicationExistence:: application:::', application);
    if (!application) {
      throw new BadIdError(
        `Application id [${id}] does not exist`,
        'id',
        id,
      );
    }
  }

  /**
   * Check the existence for Environment instance
   * @param {String} id Environment identifier
   */
  private async checkEnvironmentExistence(id: string) {
    const environment = await this.environmentRepository.get({ id });
    logger.info('checkEnvironmentExistence:: environment:::', environment);
    if (!environment) {
      throw new BadIdError(
        `Environment id [${id}] does not exist`,
        'id',
        id,
      );
    }
  }
  /**
   * Get originalIds from array of objects
   * @param data
   */
  private getOriginalIds(data: any) {
    const originalIds = [];
    data.forEach((object: any) => {
      originalIds.push(object.originalId);
    });
    return originalIds;
  }

}
