import logger from '../config/winston';
import { IEnvironment } from '../entities';
import { Nullable } from '../libs/Nullable';
import EnvironmentRepository from '../repositories/business/environment/EnvironmentRepository';

export default class EnvironmentController {
  public static getInstance() {
    if (!EnvironmentController.instance) {
      EnvironmentController.instance = new EnvironmentController();
    }

    return EnvironmentController.instance;
  }

  private static instance: EnvironmentController;

  /* tslint:disable:variable-name */
  private _environmentRepository: EnvironmentRepository;

  /* tslint:disable:no-null-keyword */
  private constructor(environmentRepository: Nullable<EnvironmentRepository> = null) {
    this._environmentRepository = environmentRepository ? environmentRepository : new EnvironmentRepository();
  }

  /**
   * Create new environment
   * @property {string} name - The technical (unique) name of the environment.
   * @property {string} caption - The displayable name of the environment.
   * @returns {IEnvironment}
   */

  public async create(name: string, caption: string): Promise<Nullable<IEnvironment>> {
    try {
      logger.debug('EnvironmentController - Create');

      const validCaption = caption ? caption : name;

      return EnvironmentController.getInstance()._environmentRepository.create({
        caption: validCaption,
        name,
      });
    } catch (e) {
      logger.error('EnvironmentController - Create exception --> ', e);
      throw (e);
    }
  }

  /**
   * Get environments list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {IEnvironment[]}
   */
  public async list(limit: number, skip: number): Promise<IEnvironment[]> {
    try {
      logger.debug('EnvironmentController - List');

      return EnvironmentController.getInstance()._environmentRepository.list({ limit, skip });
    } catch (e) {
      logger.error('EnvironmentController - List exception --> ', e);
      throw (e);

    }
  }

  /**
   * Get Environment.
   * @property {string} id - Environment unique identifier
   * @returns {IEnvironment}
   */
  public async get(id: string): Promise<Nullable<IEnvironment>> {
    try {
      logger.debug('EnvironmentController - Get');

      return EnvironmentController.getInstance()._environmentRepository.get({ id });
    } catch (e) {
      logger.error('EnvironmentController - Get exception --> ', e);
      throw (e);

    }
  }

  /**
   * Update existing environment.
   * @property {string} id - Unique identifier Id of an environment
   * @property {string} name - Updated name for the exixting environment
   * @property {string} caption - Updated caption for the existing environment
   * @returns {IEnvironment}
   */
  public async update(id: string, name?: string, caption?: string): Promise<Nullable<IEnvironment>> {
    try {
      logger.debug('EnvironmentController - Update');

      const options: any = { originalId: id };
      if (name) { options.name = name; }
      if (caption) { options.caption = caption; }
      return EnvironmentController.getInstance()._environmentRepository.update(options);
    } catch (e) {
      logger.error('EnvironmentController - Update exception --> ', e);
      throw (e);

    }
  }
}
