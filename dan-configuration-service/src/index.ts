
// config should be imported before importing any other file

import config from './config/configuration';
import Server from './Server';
import { env } from './types';

import router from './router';

const server = new Server(config);
server.bootstrap()
  .setupRoutes(router, (
    config.env === env.DEV || config.env === env.TEST
  ));

export default server;
