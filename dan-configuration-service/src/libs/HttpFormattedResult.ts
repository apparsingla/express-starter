class HttpFormattedResult {
  /**
   * Creates a result edible for http response body.
   * @param {number} status - result outcome at a glance, from a http perspective.
   * @param {string} message - a business insight about the result.
   * @param {any} data - the plain result as it comes from the business layer.
   */
  constructor(
    public status: number,
    public message: string,
    public data: any,
  ) {
    this.status = status;
    this.message = message;
    this.data = data;
  }
}

export default HttpFormattedResult;
