import { StatusCodes } from '../constants';
import BaseError from './BaseError';
import IError from './IError';

/**
 * Class representing an API error.
 * @extends BaseError
 */
class APIError extends BaseError {

  private static ERROR_TYPE = 'APIError';
  public status: number;
  public data: IError[];
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {number} status - HTTP status code of error.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(
    message: string,
    status: number = StatusCodes.INTERNAL_SERVER_ERROR,
    data: IError[] = [],
    isPublic: boolean = false,
  ) {
    super(message, isPublic, APIError.ERROR_TYPE);
    this.status = status;
    this.data = data;
  }
}

export default APIError;
