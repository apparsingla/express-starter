export default interface IError {
  msg: string;
  param: string;
  value: string;
  type: string;
}
