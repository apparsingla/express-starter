import IError from '../IError';

class BadFormatIdError implements IError {

  public static ERROR_TYPE = 'BadFormatIdError';
  public msg: string;
  public param: string;
  public value: any;
  public type: string;
  /**
   * Creates an API error.
   * @param {string} message - Error message.
   * @param {string} duplicateVariable - Variable being duplicate.
   * @param {string} duplicateValue - Value of the variable being duplicate.
   * @param {boolean} isPublic - Whether the message should be visible to user or not.
   */
  constructor(
    msg: string,
    param: string,
    value: any,
  ) {
    this.msg = msg;
    this.param = param;
    this.value = value;
    this.type = BadFormatIdError.ERROR_TYPE;
  }
}

export default BadFormatIdError;
