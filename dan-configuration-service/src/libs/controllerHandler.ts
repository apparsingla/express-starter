import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator/check';

import APIError from './errors/APIError';
import IError from './errors/IError';
import SystemResponse from './SystemResponse';

export default (promise: any, validator: any) => async (req: Request, res: Response, next: NextFunction) => {
  // Checking for errors
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(SystemResponse.validationError(errors.array() as IError[]));
  }

  // Extracting only concerned fields, to remove unwanted fields form the list
  const params: any = {};
  const body: any = {};
  const query: any = {};

  for (const field in validator) {
    if (validator.hasOwnProperty(field)) {
      const type = validator[field].in && validator[field].in[0];
      if (type) {
        switch (type) {
          case 'body':
            if (req.body[field]) {
              body[field] = req.body[field];
            }
            break;

          case 'params':
            if (req.params[field]) {
              params[field] = req.params[field];
            }
            break;

          case 'query':
            if (req.query[field]) {
              query[field] = req.query[field];
            }
            break;
        }
      }
    }
  }
  try {
    const result = await promise({
      body,
      params,
      query,
    });

    if (result.type === APIError.name) {  // result is an APIError
      next(result);
    } else {
      const responseBody = { // result is proper data
        data: result,
        metadata: {
          code: '',
          message: '',
          timestamp: new Date(),
        },
      };
      return res.json( SystemResponse.success(responseBody) );
    }
  } catch (error) {
    return next(SystemResponse.serverError([error]) );
  }
};
