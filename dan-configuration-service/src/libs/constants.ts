export const SWAGGER_URL = '/api-docs';
export const API_PREFIX = '/api';

export const ABOUT = {
  description: 'Kitchen sink API with Swagger',
  title: 'DAN Kitchen sink API',
};

// Listing of Environments
export enum EnvVars {
  DEV = 'development',
  TEST = 'test',
  PROV = 'provision',
  PROD = 'production',
}

export enum StatusCodes {
  BAD_REQUEST = 400,
  NOT_FOUND = 404,
  UNPROCESSABLE = 422,
  INTERNAL_SERVER_ERROR = 500,
}
