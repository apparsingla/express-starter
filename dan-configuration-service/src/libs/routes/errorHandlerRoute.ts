import * as httpStatus from 'http-status';

import { NextFunction, Request, Response } from 'express';

export default (stack: boolean = false) => (
  err: any,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  return res.status(err.status || httpStatus.INTERNAL_SERVER_ERROR).json({
    data: err.data || [],
    message: err.isPublic ? err.message : (httpStatus as any)[err.status],
    stack: stack ? err.stack : '',
    status: 'error',
  });
};
