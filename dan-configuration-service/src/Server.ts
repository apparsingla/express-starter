import * as bodyParser from 'body-parser';
import * as compress from 'compression';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';

import * as express from 'express';
import * as helmet from 'helmet';
import * as http from 'http';
import * as methodOverride from 'method-override';
import logger from './config/winston';

// import { AuthManager } from 'dan-auth-middleware';

import Database from './libs/Database';
import Swagger from './libs/Swagger';

import { errorHandlerRoute, notFoundRoute } from './libs/routes';
const winston = require('./config/winston');
const morgan = require('morgan');
export default class Server {
  private app: express.Express;
  private server?: http.Server;

  constructor(private config: any) {
    this.app = express();
    this.server = undefined;
  }

  get application() {
    return this.app;
  }

  /**
   * To enable all the setting on our express app
   * @returns -Instance of Current Object
   */
  public bootstrap() {

    const { authProvider } = this.config;

    this._initHelmet();
    this._initCompress();
    this._initCookieParser();
    this._initCors();
    this._initAuth(authProvider);

    this._initJsonParser();
    this._initMethodOverride();
    this._initSwagger();

    return this;
  }

  /**
   * This will run the server at specified port after opening up of Database
   *
   * @returns -Instance of Current Object
   */
  public run() {
    // open Database & listen on port config.port
    const { port, env, mongo } = this.config;
    const me = this;
    Database.open({ mongoUri: mongo, testEnv: false }).then(() => {
      this.server = this.app.listen(port, () => {
        console.info(`server started on port ${port} (${env})`); // eslint-disable-line no-console

        this._initSockets();
      });
    });

    return this;
  }

  /**
   *
   *
   * @returns Promise
   *
   */
  public testDBConnect() {
    const { mongo } = this.config;
    return Database.open({ mongoUri: mongo, testEnv: true });
  }

  /**
   * Close the connected Database
   *
   * @returns Promise
   * @memberof Server
   */
  public closeDB() {
    return Database.close();
  }

  /**
   * This will Setup all the routes in the system
   *
   * @returns -Instance of Current Object
   * @memberof Server
   */
  public setupRoutes(router: express.Router, stack: boolean = false) {

    logger.info('Initialising routes...');

    const { apiPrefix } = this.config;

    // mount all routes on /api path
    this.app.use(apiPrefix, router);

    // catch 404 and forward to error handler
    this.app.use(notFoundRoute);

    this.app.use(morgan('combined', { stream: winston.stream }));

    // error handler, send stacktrace only during development
    this.app.use(errorHandlerRoute(stack));

    return this;
  }

  /**
   * Compression of the output
   */
  private _initCompress() {

    logger.info('Initialising Compress...');

    this.app.use(compress());
  }

  /**
   * Parse Cookie header and populate req.cookies with an object keyed by the cookie names
   */
  private _initCookieParser() {
    logger.info('Initialising CookieParser...');

    this.app.use(cookieParser());
  }

  /**
   *
   * Lets you to enable cors
   */
  private _initCors() {

    logger.info('Initialising Cors...');

    this.app.use(cors());
  }

  /**
   *
   * Helmet helps you secure your Express apps by setting various HTTP headers.
   */
  private _initHelmet() {

    logger.info('Initialising Helmet...');

    this.app.use(helmet());
  }

  /**
   *  - Parses urlencoded bodies & JSON
   */
  private _initJsonParser() {

    logger.info('Initialising JsonParser...');

    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
  }

  /**
   *
   * Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it.
   */
  private _initMethodOverride() {

    logger.info('Initialising MethodOverride...');

    this.app.use(methodOverride());
  }

  /**
   * Initialize auth manager
   */
  private _initAuth(authProvider) {

    logger.info('Starting Auth Middleware...');

    // const authManager: AuthManager = AuthManager.getInstance();
    // authManager.init(authProvider);
  }

  private _initSwagger() {

    logger.info('Starting Swagger...');

    const { swaggerDefinition, swaggerUrl } = this.config;
    const swaggerSetup = new Swagger();
    // JSON route
    this.app.use(`${swaggerUrl}.json`, swaggerSetup.getRouter({
      swaggerDefinition,
    }));

    // UI route
    const { serve, setup } = swaggerSetup.getUI(swaggerUrl);

    this.app.use(swaggerUrl, serve, setup);
  }

  private _initSockets() {

    logger.info('Starting Sockets...');

    const io = require('socket.io')(this.server);

    io.on('connection', (socket: any) => {
      logger.debug('Connection detected');
      socket.on('disconnect', () => {
        logger.debug('Disconnection detected');
      });
    });
  }
}
