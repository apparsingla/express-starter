import IEntity from './IEntity';

export default interface IEnvironment extends IEntity {
  name: string;
  caption: string;
}
