import IEntity from './IEntity';

export default interface IUser extends IEntity {
  name: string;
}
