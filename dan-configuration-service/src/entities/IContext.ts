import IEntity from './IEntity';

export default interface IContext extends IEntity {
  url: string;
  application: string;
  environment: string;
}
