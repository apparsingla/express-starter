import IEntity from './IEntity';

export default interface IConfiguration extends IEntity {
  context: string;
  fields: string[] | any[];
}
