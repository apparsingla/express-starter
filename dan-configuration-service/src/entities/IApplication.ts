import IEntity from './IEntity';

export default interface IApplication extends IEntity {
  name: string;
  caption: string;
}
