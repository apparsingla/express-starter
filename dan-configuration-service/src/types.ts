import { ISwaggerDefinition } from './libs/Swagger';

export const SWAGGER_URL: string = '/api-docs';
export const API_PREFIX: string = '/api';

export const ABOUT = {
  description: 'Config Manager API with Swagger',
  title: 'DAN Config Manager API',
};

// Listing of Environments
export const env = Object.freeze({
  DEV: 'dev',
  PROD: 'production',
  PROV: 'provision',
  TEST: 'test',
});

export interface IConfig extends ISwaggerDefinition {
  authProvider: IAuthProviderConfig;
  env: string;
  port: string;
  mongooseDebug: boolean;
  mongo: string;
  swaggerUrl: string;
  apiPrefix: string;
}

export interface IList {
  limit: number;
  skip: number;
}

export interface IList {
  limit: number;
  skip: number;
}
export interface IRule {
  route: string;
  methods?: string[];
  allow: string;
}
export interface IAuthenticationConfig {
  issuer: string;
  clientId: string;
}
export interface IAuthorizationConfig {
  getAuthorizations: boolean;
  authorizationEndpoint: string;
  allRolesEndpoint: string;
  prefetchUserRoles?: string[];
  application: string;
  openDomain?: boolean;
  allowWhenNoRule?: boolean;
  rules?: IRule[];
}
export interface IAuthProviderConfig {
  active: boolean;
  authentication: IAuthenticationConfig;
  authorization: IAuthorizationConfig;
}
