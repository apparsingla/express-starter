import { API_PREFIX } from '../types';

import * as request from 'supertest';
import server from '../';

describe('Health Check', () => {
  it('should return 404 ', (done) => {
    request(server.application).get('/fake-url')
      .expect(404, done);
  });

  it('should return 200', (done) => {
    request(server.application).get(`${API_PREFIX}/health-check`)
      .expect(200, done);
  });

});
