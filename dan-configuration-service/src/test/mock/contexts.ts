const contextList = {
  badApplicationId: {
    application: 'badapplicationid',
    url: 'http://localhost:3000',
  },
  badEnvironmentId: {
    environment: 'badenvironmentid',
    url: 'http://localhost:3000',
  },
  badFormatURL: {
    url: 'bad-format-url',
  },
  rightContext: {
    application: '',
    environment: '',
    url: 'http://localhost:3000',
  },
  rightContextUpdate: {
    application: '',
    environment: '',
    id: '',
    url: 'http://localhost:1000',
  },
};

export default contextList;
