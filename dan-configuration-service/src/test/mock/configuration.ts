export const badIdOrContextFormat = '0000000000abcdefghjk0101';
export const badConfigBodyFormat = [{ name: 'test case' }];
export const context = '000000000000000000000000';

export const configurationList = {
  badFormatConfigurationBodyForCreate: {
    configurationBody: badConfigBodyFormat,
    context,
  },
  badFormatConfigurationBodyForUpdate: {
    configurationBody: badConfigBodyFormat,
    id: context,
  },
  badFormatContext: {
    configurationBody: {
      arrayType: [
        'test case',
        {
          arrName: 'configuration service',
        },
      ],
      boolType: true,
      numeralType: 1010,
      objectType: {
        objName: 'configuration service',
      },
      stringType: 'testcase:27017/config-testing',
    },
    context: badIdOrContextFormat,
  },
  badFormatId: {
    configurationBody: {
      arrayType: [
        'test case',
        {
          arrName: 'configuration service',
        },
      ],
      boolType: true,
      numeralType: 1010,
      objectType: {
        objName: 'configuration service',
      },
      stringType: 'testcase:27017/config-testing',
    },
    id: badIdOrContextFormat,
  },
  dataForUpdate: {
    boolType: false,
    numeralType: 1234,
    objectType: {
      objName: 'configuration service',
    },
    stringType: 'testcase:27017/config-testing',
  },
  withAllParameters: {
    configurationBody: {
      arrayType: [
        'test case',
        {
          arrName: 'configuration service',
        },
      ],
      boolType: true,
      numeralType: 1010,
      objectType: {
        objName: 'configuration service',
      },
      stringType: 'testcase:27017/config-testing',
    },
    context: '',
  },
  withNonExistingContext: {
    configurationBody: {
      arrayType: [
        'test case',
        {
          arrName: 'configuration service',
        },
      ],
      boolType: true,
      numeralType: 1010,
      objectType: {
        objName: 'configuration service',
      },
      stringType: 'testcase:27017/config-testing',
    },
    context,
  },
  withoutConfigurationBodyForCreate: {
    context,
  },
  withoutConfigurationBodyForUpdate: {
    context,
  },
  withoutContextOrId: {
    configurationBody: {
      arrayType: [
        'test case',
        {
          arrName: 'configuration service',
        },
      ],
      boolType: true,
      numeralType: 1010,
      objectType: {
        objName: 'configuration service',
      },
      stringType: 'testcase:27017/config-testing',
    },
  },
};
