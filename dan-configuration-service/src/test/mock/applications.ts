const applicationList = {
  badFormatCaption: {
    caption: 'bad&format&caption',
    name: 'name1',
  },
  badFormatName: {
    caption: 'caption1',
    name: 'bad-format-name',
  },
  rightApplication: {
    caption: 'caption1',
    name: 'name1',
  },
  rightApplicationContext: {
    caption: 'captioncontext',
    name: 'namecontext',
  },
};

export default applicationList;
