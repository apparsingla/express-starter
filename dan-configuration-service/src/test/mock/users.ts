export const usersList = {
  badFormatUser: {
    password: 'm!ghty_M33ty',
    userName: 'bad.format.user',
  },
  baseUser: {
    password: 'm!ghtyM33ty',
    userName: 'Tvstack.user1@dentsuaegis.com',
  },
  clientAdmin: {
    password: 'm!ghtyM33ty',
    userName: 'client.admin1@dentsuaegis.com',
  },
  globalAdmin: {
    password: 'm!ghtyM33ty',
    userName: 'global.admin1@dentsuaegis.com',
  },
  marketAdmin: {
    password: 'm!ghtyM33ty',
    userName: 'Market.admin1@dentsuaegis.com',
  },
  wrongUser: {
    password: 'm!ghtyM33ty',
    userName: 'wrong.user@dentsuaegis.com',
  },
};
