const environmentList = {
  badFormatCaption: {
    caption: 'bad&format&caption',
    name: 'name1',
  },
  badFormatName: {
    caption: 'caption1',
    name: 'bad-format-name',
  },
  rightEnvironment: {
    caption: 'caption1',
    name: 'name1',
  },
  rightEnvironmentContext: {
    caption: 'captioncontext',
    name: 'namecontext',
  },
};

export default environmentList;
