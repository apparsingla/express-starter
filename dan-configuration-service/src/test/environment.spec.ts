import * as request from 'supertest';
import TestToolbox from './TestToolBox';

import server from '../';
import { StatusCodes } from '../libs/constants';
import SystemResponse from '../libs/SystemResponse';
import { environmentModel } from '../repositories/business/environment/environmentModel';
import { API_PREFIX } from '../types';
import environmentList from './mock/environments';

describe('Environment Scene', () => {
  let environmentId = '';
  beforeAll((done) => {
    jest.setTimeout(20000);
    // Opening of DB connection and clearing the Database
    return server.testDBConnect().then(async() => {
      await environmentModel.remove({});
      done();
    });
  });

  afterAll(() => {
    // Closing of DB connection
    return server.closeDB();
  });

  describe('Environment Create', () => {
    it('should insert the record', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/environments`)
        .set('Authorization', token)
        .send(environmentList.rightEnvironment)
        .expect('Content-Type', /json/)
        .expect(200)
        .then((res) => {
          environmentId = res.body.data.id;
          expect(res.body.data.name).toBe(environmentList.rightEnvironment.name);
          expect(res.body.data.caption).toBe(environmentList.rightEnvironment.caption);
        });
    });

    it('should throw validation error: caption is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send(environmentList.badFormatCaption)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });


    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send(environmentList.badFormatName)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

  });

  describe('Environment Fetch', () => {
    it('should return 1 element - default', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get('/api/environments')
        .set('Authorization', token)
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(1);
        });
    });

    it('should return 0 element - skip by 1', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get('/api/environments')
        .set('Authorization', token)
        .query({ skip: 1 })
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(0);
        });
    });

    it('should return validation error for providing bad skip value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/environments?skip=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('skip count is wrong');
        });
    });

    it('should return validation error for providing bad limit value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/environments?limit=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('limit is wrong');
        });
    });
  });

  describe('Environment Update', () => {
    it('should update the record', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ caption: environmentList.badFormatCaption.caption, id: environmentId, name: 'testnew' })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });

    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ name: environmentList.badFormatName.name, id: environmentId })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error: caption is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ caption: environmentList.badFormatCaption.caption, id: environmentId, name: 'testnew' })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });

    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ name: environmentList.badFormatName.name, id: environmentId })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error: caption is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ caption: environmentList.badFormatCaption.caption, id: environmentId, name: 'testnew' })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });

    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ name: environmentList.badFormatName.name, id: environmentId })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/environments`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });
  });
});
