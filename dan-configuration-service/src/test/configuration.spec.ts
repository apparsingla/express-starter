import * as request from 'supertest';
import TestToolbox from './TestToolBox';

import server from '../';
import { StatusCodes } from '../libs/constants';
import SystemResponse from '../libs/SystemResponse';
import { applicationModel } from '../repositories/business/application/applicationModel';
import ApplicationRepository from '../repositories/business/application/ApplicationRepository';
import { configurationModel } from '../repositories/business/configuration/configurationModel';
import ConfigurationRepository from '../repositories/business/configuration/ConfigurationRepository';
import { contextModel } from '../repositories/business/context/contextModel';
import ContextRepository from '../repositories/business/context/ContextRepository';
import { environmentModel } from '../repositories/business/environment/environmentModel';
import EnvironmentRepository from '../repositories/business/environment/EnvironmentRepository';
import { API_PREFIX } from '../types';
import applicationList from './mock/applications';
import { configurationList } from './mock/configuration';
import contextList from './mock/contexts';
import environmentList from './mock/environments';

describe('Configuration Scene', () => {
  const applicationRepository = new ApplicationRepository();
  const environmentRepository = new EnvironmentRepository();
  const contextRepository = new ContextRepository();
  let application: any = '';
  let environment: any = '';
  let context: any = '';
  let applicationId = '';
  let environmentId = '';
  let configurationId = '';

  beforeAll(async (done) => {
    jest.setTimeout(20000);
    // Opening of DB connection and clearing the Database
    return server.testDBConnect().then(async () => {
      await configurationModel.remove({});
      await applicationModel.remove({});
      await environmentModel.remove({});
      await contextModel.remove({});
      application = await applicationRepository.create(applicationList.rightApplicationContext);
      applicationId = application.id;
      environment = await environmentRepository.create(environmentList.rightEnvironmentContext);
      environmentId = environment.id;
      contextList.rightContext.application = application.originalId;
      contextList.rightContext.environment = environment.originalId;
      context = await contextRepository.create(contextList.rightContext);
      configurationList.withAllParameters.context = context.id;
      done();
    });
  });

  afterAll(() => {
    // Closing of DB connection
    return server.closeDB();
  });

  describe('Create', () => {
    it('should insert the record', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Authorization', token)
        .send(configurationList.withAllParameters)
        .expect('Content-Type', /json/)
        .expect(200)
        .then((res) => {
          configurationId = res.body.data.id;
          expect(res.body.data.context).toBe(context.id);
          expect(res.body.data.fields.length).toBe(6);
        });
    });

    it('should throw BadIdError for saving configuration of non-existing context', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Authorization', token)
        .send(configurationList.withNonExistingContext)
        .expect('Content-Type', /json/)
        .expect(500)
        .then((res) => {
          expect(res.body.data[0].msg)
          .toBe(
            `Context id [${configurationList.withNonExistingContext.context}] does not exist`,
          );
        });
    });

    it('should throw duplicate error for saving configuration of same context', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Authorization', token)
        .send(configurationList.withAllParameters)
        .expect('Content-Type', /json/)
        .expect(400)
        .then((res) => {
          expect(res.body.data[0].msg)
          .toBe(
            `Configuration for context ${configurationList.withAllParameters.context} already exists. Cannot be duplicated`,
          );
        });
    });

    it('should throw validation error for not providing context', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.withoutContextOrId)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].param).toBe('context');
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error for not providing proper format of context', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.badFormatContext)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].msg).toBe('ID Bad Format');
          expect(res.body.data[0].value).toBe(configurationList.badFormatContext.context);
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error for not providing configuration body', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.withoutConfigurationBodyForCreate)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].param).toBe('configurationBody');
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error for not providing proper format of configuration body', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.badFormatConfigurationBodyForCreate)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].msg).toBe('Configuration Body is wrong!');
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error for not providing any data', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send({})
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });
  });

  describe('Fetch', () => {
    it('should return 1 element - default', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get('/api/configurations')
        .set('Authorization', token)
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(1);
        });
    });

    it('should return 1 element after giving applicationId in query', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?application=${applicationId}`)
        .set('Authorization', token)
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(1);
        });
    });

    it('should return 1 element after giving environmentId in query', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?environment=${environmentId}`)
        .set('Authorization', token)
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(1);
        });
    });

    it('should return 0 element - skip by 1', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get('/api/configurations')
        .set('Authorization', token)
        .query({ skip: 1 })
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(0);
        });
    });

    it('should return error after giving non-existing applicationId in query', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?application=123456789123456789123456`)
        .set('Authorization', token)
        .expect(500)
        .then((res) => {
          expect(res.body.data[0].msg).toBe(
            'Application id [123456789123456789123456] does not exist',
          );
        });
    });

    it('should return error after giving non-existing environmentId in query', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?environment=123456789123456789123456`)
        .set('Authorization', token)
        .expect(500)
        .then((res) => {
          expect(res.body.data[0].msg).toBe(
            'Environment id [123456789123456789123456] does not exist',
          );
        });
    });

    it('should return validation error for providing bad skip value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?skip=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('skip count is wrong');
        });
    });

    it('should return validation error for providing bad limit value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?limit=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('limit is wrong');
        });
    });

    it('should return validation error for providing bad environmentId in query', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?environment=qwertyuiopqwertyuiopqwer`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('qwertyuiopqwertyuiopqwer');
          expect(res.body.data[0].msg).toBe('Environment is wrong!');
        });
    });

    it('should return validation error for providing bad applicationId in query', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/configurations?application=qwertyuiopqwertyuiopqwer`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('qwertyuiopqwertyuiopqwer');
          expect(res.body.data[0].msg).toBe('Application is wrong!');
        });
    });
  });

  describe('Update', () => {
    it('should update the configuration', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/configurations`)
        .set('Authorization', token)
        .send({configurationBody: configurationList.dataForUpdate, id: configurationId})
        .expect('Content-Type', /json/)
        .expect(200)
        .then((res) => {
          expect(res.body.data.context).toBe(context.id);
        });
    });

    it('should throw validation error for not providing id', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.withoutContextOrId)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].param).toBe('id');
          expect(res.body.data[0].msg).toBe('ID Bad Format');
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error for not providing proper format of id', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.badFormatId)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].msg).toBe('ID Bad Format');
          expect(res.body.data[0].value).toBe(configurationList.badFormatId.id);
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error for not providing configuration body', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.withoutConfigurationBodyForUpdate)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].param).toBe('configurationBody');
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error for not providing proper format of configuration body', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/configurations`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .send(configurationList.badFormatConfigurationBodyForUpdate)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].msg).toBe('Configuration Body is wrong!');
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should return error while updating the configuration without giving any data', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/configurations`)
        .set('Authorization', token)
        .send({})
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.message).toBe('Unprocessable Entity');
        });
    });
  });
});
