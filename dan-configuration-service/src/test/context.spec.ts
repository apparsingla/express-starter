import * as request from 'supertest';
import TestToolbox from './TestToolBox';

import server from '../';
import { StatusCodes } from '../libs/constants';
import SystemResponse from '../libs/SystemResponse';
import { applicationModel } from '../repositories/business/application/applicationModel';
import ApplicationRepository from '../repositories/business/application/ApplicationRepository';
import { contextModel } from '../repositories/business/context/contextModel';
import { environmentModel } from '../repositories/business/environment/environmentModel';
import EnvironmentRepository from '../repositories/business/environment/EnvironmentRepository';
import { API_PREFIX } from '../types';
import applicationList from './mock/applications';
import contextList from './mock/contexts';
import environmentList from './mock/environments';


describe('Context Scene', () => {
  const applicationRepository = new ApplicationRepository();
  const environmentRepository = new EnvironmentRepository();
  let application: any = '';
  let environment: any = '';
  beforeAll((done) => {
    jest.setTimeout(20000);
    // Opening of DB connection and clearing the Database
    return server.testDBConnect().then(async () => {
      await applicationModel.remove({});
      await environmentModel.remove({});
      application = await applicationRepository.create(applicationList.rightApplicationContext);
      environment = await environmentRepository.create(environmentList.rightEnvironmentContext);
      contextList.rightContext.application = application.originalId;
      contextList.rightContext.environment = environment.originalId;
      contextList.rightContextUpdate.application = application.originalId;
      contextList.rightContextUpdate.environment = environment.originalId;
      await contextModel.remove({});
      done();
    });
  });

  afterAll(() => {
    // Closing of DB connection
    return server.closeDB();
  });

  describe('Context Create', () => {
    it('should insert the record', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/contexts`)
        .send(contextList.rightContext)
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(200)
        .then((res) => {
          contextList.rightContextUpdate.id = res.body.data.id;
          expect(res.body.data.url).toBe(contextList.rightContext.url);
          expect(res.body.data.application).toBe(contextList.rightContext.application);
          expect(res.body.data.environment).toBe(contextList.rightContext.environment);
        });
    });

    it('should throw validation error: bad application id', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/contexts`)
        .send(contextList.badApplicationId)
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error: bad environment id', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/contexts`)
        .send(contextList.badEnvironmentId)
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });

    it('should throw validation error', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();

      return request(server.application)
        .post(`${API_PREFIX}/contexts`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });
  });

  describe('Context Fetch', () => {
    it('should return 1 element - default', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();

      return request(server.application)
        .get('/api/contexts')
        .set('Authorization', token)
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(1);
        });
    });

    it('should return 0 element - skip by 1', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();

      return request(server.application)
        .get('/api/contexts')
        .set('Authorization', token)
        .query({ skip: 1 })
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(0);
        });
    });

    it('should return validation error for providing bad skip value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/contexts?skip=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('skip count is wrong');
        });
    });

    it('should return validation error for providing bad limit value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/contexts?limit=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('limit is wrong');
        });
    });
  });

  describe('Context Update', () => {
    it('should update the record', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/contexts`)
        .set('Authorization', token)
        .send(
          contextList.rightContextUpdate,
        )
        .expect('Content-Type', /json/)
        .expect(200)
        .then((res) => {
          expect(res.body.data.url).toBe(contextList.rightContextUpdate.url);
          expect(res.body.data.application).toBe(contextList.rightContextUpdate.application);
          expect(res.body.data.environment).toBe(contextList.rightContextUpdate.environment);
        });
    });

    it('should throw validation error', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/contexts`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });
  });
});
