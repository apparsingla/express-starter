import configurations from '../config/configuration';
import { usersList } from './mock/users';

const CLIENT_ID = '0oa1s3viyri4VMOfG0i7';
const REDIRECT_URI = 'http://localhost:3010/api/users/implicit/callback';

export default class TestToolbox {
    public static getInstance(): TestToolbox {
        if (!TestToolbox.instance) {
            TestToolbox.instance = new TestToolbox();
        }
        return TestToolbox.instance;
    }
    private static instance: TestToolbox;

    public async getGlobalAdminJWT() {
        return await this.getTestJWT(usersList.globalAdmin.userName, usersList.globalAdmin.password);
    }

    public async getMarketAdminJWT() {
        return await this.getTestJWT(usersList.marketAdmin.userName, usersList.marketAdmin.password);
    }

    public async getClientAdminJWT() {
        return await this.getTestJWT(usersList.clientAdmin.userName, usersList.clientAdmin.password);
    }

    public async getUserJWT() {
        return await this.getTestJWT(usersList.baseUser.userName, usersList.baseUser.password);
    }

    private async getTestJWT(user, password) {

        const fetch = require('isomorphic-fetch');

        const authnRes = await fetch(`https://dentsuaegis-test.okta-emea.com/api/v1/authn`, {
            body: JSON.stringify({
                password,
                username: user,
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method: 'POST',
        });

        const trans = await authnRes.json();

        try {
            // configurations.authProvider.authentication.issuer;
            const authorizeRes = await fetch(
                `${configurations.authProvider.authentication.issuer}/v1/authorize?` +
                'response_type=token&' +
                'scope=openid&' +
                'state=TEST&' +
                `nonce=TEST1&` +
                `client_id=${CLIENT_ID}&` +
                `redirect_uri=${REDIRECT_URI}&` +
                `sessionToken=${trans.sessionToken}`);

            const accessToken = this.parseRedirectURL(authorizeRes.url);
            return accessToken;

        } catch (err) {
            const errorMessage = err.message;
            const startIndex = errorMessage.indexOf('request to') + 'request to'.length;
            const endIndex = errorMessage.substr(startIndex).indexOf(' failed') > -1 ?
                errorMessage.substr(startIndex).indexOf(' failed') + startIndex : errorMessage.length;
            const url = errorMessage.substring(startIndex, endIndex);
            const accessToken = this.parseRedirectURL(url);
            return accessToken;
        }

    }

    private parseRedirectURL(url) {

        const startIndex = url.indexOf('access_token=') + 'access_token='.length;
        const endIndex = url.substr(startIndex).indexOf('&') > -1 ?
            url.substr(startIndex).indexOf('&') + startIndex : url.length;

        return 'Bearer ' + url.substring(startIndex, endIndex);
    }
}
