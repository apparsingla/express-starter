
import * as request from 'supertest';
import TestToolbox from './TestToolBox';

import server from '../';
import { StatusCodes } from '../libs/constants';
import SystemResponse from '../libs/SystemResponse';
import { applicationModel } from '../repositories/business/application/applicationModel';
import { API_PREFIX } from '../types';
import applicationList from './mock/applications';

describe('Application Scene', () => {
  let applicationId: string = '';
  beforeAll((done) => {
    jest.setTimeout(20000);
    // Opening of DB connection and clearing the Database
    return server.testDBConnect().then(async () => {
      await applicationModel.remove({});
      done();
    });
  });

  afterAll(() => {
    // Closing of DB connection
    return server.closeDB();
  });

  describe('Application Create', () => {
    it('should insert the record', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send(applicationList.rightApplication)
        .expect('Content-Type', /json/)
        .expect(200)
        .then((res) => {
          applicationId = res.body.data.id;
          expect(res.body.data.name).toBe(applicationList.rightApplication.name);
        });
    });

    it('should throw validation error: caption is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send(applicationList.badFormatCaption)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });

    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send(applicationList.badFormatName)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });
  });

  describe('Application Fetch', () => {
    it('should return 1 element - default', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get('/api/applications')
        .set('Authorization', token)
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(1);
        });
    });

    it('should return 0 element - skip by 1', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get('/api/applications')
        .set('Authorization', token)
        .query({ skip: 1 })
        .expect(200)
        .then((res) => {
          expect(res.body.data.length).toBe(0);
        });
    });

    it('should return validation error for providing bad skip value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/applications?skip=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('skip count is wrong');
        });
    });

    it('should return validation error for providing bad limit value', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .get(`/api/applications?limit=io`)
        .set('Authorization', token)
        .expect(422)
        .then((res) => {
          expect(res.body.data[0].value).toBe('io');
          expect(res.body.data[0].msg).toBe('limit is wrong');
        });
    });

  });

  describe('Application Update', () => {
    it('should update the record', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ caption: applicationList.badFormatCaption.caption, id: applicationId, name: 'testnew' })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });

    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ name: applicationList.badFormatName.name, id: applicationId })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error: caption is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ caption: applicationList.badFormatCaption.caption, id: applicationId, name: 'testnew' })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });

    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ name: applicationList.badFormatName.name, id: applicationId })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error: caption is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ caption: applicationList.badFormatCaption.caption, id: applicationId, name: 'testnew' })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Caption is wrong!',
          );
        });
    });

    it('should throw validation error: name is wrong', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .post(`${API_PREFIX}/applications`)
        .set('Authorization', token)
        .send({ name: applicationList.badFormatName.name, id: applicationId })
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
          expect(res.body.data[0].msg).toMatch(
            'Name is wrong!',
          );
        });
    });

    it('should throw validation error', async () => {
      const token: any = await TestToolbox.getInstance().getUserJWT();
      return request(server.application)
        .put(`${API_PREFIX}/applications`)
        .set('Accept', 'application/json')
        .set('Authorization', token)
        .expect('Content-Type', /json/)
        .expect(422)
        .then((res) => {
          expect(res.body.stack).toMatch(
            SystemResponse.getStatusCodes(StatusCodes.UNPROCESSABLE).message,
          );
        });
    });
  });
});
