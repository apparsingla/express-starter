// import * as logger from 'dan-logger';
import * as mongoose from 'mongoose';
import { DocumentQuery } from 'mongoose';
import IApplication from '../entities/IApplication';
import { Nullable } from '../libs/Nullable';
import { IQueryBaseCreate, IQueryBaseGet, IQueryBaseList, IQueryBaseUpdate } from './entities';

export default abstract class BaseRepository<D extends mongoose.Document, M extends mongoose.Model<D>> {

  public static generateObjectId() {
    return String(mongoose.Types.ObjectId());
  }

  public static checkId(id: string) {
    return mongoose.Types.ObjectId.isValid(id);
  }

  public static checkHexadecimal(id: string) {
    const idPattern = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
    return idPattern.test(id);
  }

  public static checkEmail(email: string) {
    /* tslint:disable:max-line-length */
    const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailPattern.test(String(email).toLowerCase());
  }

  public static checkForInvalidCharacters(token: string) {
    const specialCharsPattern = /[&'"\\\/?]/;
    return !specialCharsPattern.test(token);
  }

  public static checkAlphanum(token: string) {
    const checkAlphanumPattern = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    return !checkAlphanumPattern.test(token);
  }

  public static checkForInvalidURL(url: string) {
    const validURLPattern = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;
    return !validURLPattern.test(url);
  }

  public static checkForValidJSON(option: object) {
    if ((Object.prototype.toString.call(option) === '[object Object]') && (Object.keys(option).length > 0)) {
      return true;
    }
    return false;
  }

  public static isJSONString(option: string) {
    try {
        JSON.parse(option);
    } catch (e) {
        return false;
    }
    return true;
  }

  public static isValidJSON(option: object) {
    try {
      JSON.stringify(option);
    } catch (e) {
        return false;
    }
    return true;
  }

  protected modelType: M;

  constructor(modelType) {
    this.modelType = modelType;
  }
  /**
   * Get application list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {Application[]}
   */
  public async list(query: IQueryBaseList): Promise<D[]> {
    const { skip, limit } = query;
    delete query.limit;
    delete query.skip;
    return this.getAll(query, { skip, limit, sort: { createdAt: -1 } });
  }

  /**
   * Get application.
   * @property {string} id - _id of the record
   * @returns {Application}
   */
  public async get(options: IQueryBaseGet): Promise<Nullable<D>> {
    return this.getById(options.id);
  }

  /**
   * Get application.
   * @property {string} id - _id of the record
   * @returns {Application}
   */
  // public async delete(options: IQueryBaseGet): Promise<Nullable<D>> {
  //   return this.deleteById(options.id);
  // }

  /**
   * Create new application
   * @property {string} body.name - The name of record.
   * @returns {Application}
   */
  public async create(options: IQueryBaseCreate): Promise<D> {

    const id = BaseRepository.generateObjectId();
    const model = new this.modelType({
      ...options,
      _id: id,
    });

    return await model.save();
  }

  protected getAll(query: any, projection: any = { }, options: any = {}): DocumentQuery<D[], D> {
    return this.modelType.find(query, projection, options);
  }
  protected getById(id: string): DocumentQuery<D | null, D> {
    return this.modelType.findById(id);
  }
  protected deleteById(id: string): DocumentQuery<D | null, D> {
    return this.deleteById(id);
  }
  protected getByIds(ids: string[]): DocumentQuery<D[], D> {
    return this.getAll({ _id: { $in: ids } });
  }

}
