import * as mongoose from 'mongoose';
import { DocumentQuery } from 'mongoose';
import IApplication from '../../entities/IApplication';
import { Nullable } from '../../libs/Nullable';
import BaseRepository from '../BaseRepository';
import { IQueryBaseCreate, IQueryBaseGet, IQueryBaseList, IQueryBaseUpdate, IQueryBaseDelete } from '../entities';
import logger from './../../config/winston';
import IVersionableDocument from './IVersionableDocument';

export default class VersioningRepository<D extends mongoose.Document, M extends mongoose.Model<D>>
  extends BaseRepository<D, M> {

  // private modelType: M;

  constructor(modelType) {
    super(modelType);
  }

  /**
   * Create new application
   * @property {string} body.name - The name of record.
   * @returns {Application}
   */
  public async create(options: IQueryBaseCreate): Promise<D> {
    const id = VersioningRepository.generateObjectId();
    const model = new this.modelType({
      ...options,
      _id: id,
      originalId: id,
    });
    return await model.save();
  }

  /**
   * Create new application
   * @property {string} id - Record unique identifier.
   * @returns {Application}
   */
  public async update(options: IQueryBaseUpdate): Promise<D> {

    const now = new Date();
    logger.debug('Searching for previous valid object...');
    const previous = await this.getById(options.originalId);
    logger.debug('PREVIOUS::::::::', JSON.stringify(previous));
    logger.debug('Invalidating previous valid object...');
    await this.invalidate(options.originalId);

    const newInstance = Object.assign(previous.toJSON(), options);
    newInstance._id = VersioningRepository.generateObjectId();
    logger.debug('NEW INSTANCE::::::::', newInstance);
    delete newInstance.deletedAt;

    const model = new this.modelType(newInstance);

    logger.debug('Creating new object...');
    return await model.save();

  }
  public async delete(options: IQueryBaseDelete): Promise<D> {
    return await this.invalidate(options.id);
  }
  protected getAll(query: any = {} , options: any = {}): DocumentQuery<D[], D> {
    options.limit = options.limit || 0;
    options.skip = options.skip || 0;
    query.deletedAt = undefined;
    logger.debug('getAll query: ', query);
    logger.debug('getAll options: ', options);
    // tslint:disable:no-null-keyword
    return this.modelType.find(query, null, options);
  }

  protected getByQuery(query: any): DocumentQuery<D | null, D> {
    return this.modelType.findOne(query);
  }

  protected getById(id: string): DocumentQuery<D | null, D> {
    logger.debug(id);
    return this.modelType.findOne({ originalId: id, deletedAt: null });
  }

  protected getByIds(ids: string[]): DocumentQuery<D[], D> {
    return this.getAll({ originalId: { $in: ids } });
  }

  protected invalidate(id: string): DocumentQuery<D, D> {
    const now = new Date();
    return this.modelType.update({ originalId: id, deletedAt: null }, { deletedAt: now });
  }

}
