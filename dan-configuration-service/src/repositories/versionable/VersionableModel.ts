import * as mongoose from 'mongoose';

export default class VersionableModel extends mongoose.Model {

  constructor(options: any) {
    super(options);
  }
}
