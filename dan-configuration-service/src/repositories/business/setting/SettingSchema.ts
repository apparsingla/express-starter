import VersionableSchema from '../../versionable/VersionableSchema';
/**
 * Setting Schema
 */
/**
 * @swagger
 * definitions:
 *   Setting:
 *     properties:
 *       createdAt:
 *         type: date
 *         description: timestamp for the object being created
 *       deletedAt:
 *         type: date
 *         description: timestamp for the object being invalidated by a delete or update action
 *       objectId:
 *         type: string
 *         description: the unique identifier of the first instance of the record
 *       name:
 *         type: string
 *         description: a caption identifying the variable despite the configuration it belongs to
 *                      (example dev_connectionString might hold the same value across different applications in dev)
 *       value:
 *         type: object
 *         description: the setting value, compliant with the type parameter
 *       type:
 *         type: string
 *         description: a string identifying a literal type (String, Number, etc) or a proper object (example Rule)
 */
export default class SettingSchema extends VersionableSchema {

  constructor(options: any) {
    const baseSchema = {
      name: {
        required: true,
        type: String,
      },
      type: {
        required: true,
        type: String,
      },
      value: {
        required: true,
        type: Object,
      },
    };

    super(baseSchema, options);
  }

}
