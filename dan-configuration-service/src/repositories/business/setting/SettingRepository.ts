import * as mongoose from 'mongoose';
import logger from '../../../config/winston';
import { DuplicateError } from '../../../libs/errors/data';
import { Nullable } from '../../../libs/Nullable';
import VersioningRepository from '../../versionable/VersioningRepository';
import { IQueryCreate, IQueryDelete, IQueryGet, IQueryList, IQueryUpdate } from './entities';
import ISettingModel from './ISettingModel';
import { settingModel } from './settingModel';

export default class SettingRepository extends VersioningRepository<ISettingModel, mongoose.Model<ISettingModel>> {

  constructor() {
    super(settingModel);
  }

  /**
   * Get setting list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {Setting[]}
   */
  public async list(query: IQueryList): Promise<ISettingModel[]> {

    logger.debug('SettingRepository - List');

    return super.list(query);
  }

  /**
   * Get setting.
   * @property {string} id - _id of the record
   * @returns {Setting}
   */
  public async get(query: IQueryGet): Promise<Nullable<ISettingModel>> {

    logger.debug('SettingRepository - Get');

    return super.get(query);
  }

  /**
   * Create new setting
   * @property {string} name - The name of the setting variable.
   * @property {string} type - The type of the setting variable.
   * @property {object} value - The value of the setting variable.
   * @returns {Setting}
   */
  public async create(options: IQueryCreate): Promise<ISettingModel> {

    logger.debug('SettingRepository - Create');

    return super.create(options);

  }

  /**
   * Create new setting
   * @property {string} id - Record unique identifier.
   * @property {string} name - The name of the setting variable.
   * @property {string} type - The type of the setting variable.
   * @property {object} value - The value of the setting variable.
   * @returns {Setting}
   */
  public async update(options: IQueryUpdate): Promise<ISettingModel> {

    logger.debug('SettingRepository - Update');

    return super.update(options);

  }

  /**
   * Delete setting.
   * @property {string} id - _id of the record
   * @returns {Setting}
   */
  public async delete(query: IQueryDelete): Promise<Nullable<ISettingModel>> {

    logger.debug('SettingRepository - Delete');

    return super.invalidate(query.id);
  }

}
