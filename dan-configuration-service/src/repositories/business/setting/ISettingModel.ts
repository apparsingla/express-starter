import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface ISettingModel extends IVersionableDocument {
  id: string;
  name: string;
  caption: string;
}
