import * as mongoose from 'mongoose';

import ISettingModel from './ISettingModel';
import SettingSchema from './SettingSchema';

/**
 * Setting Schema
 */
/**
 * @swagger
 * definitions:
 *   Setting:
 *     properties:
 *       name:
 *         type: string
 *       type:
 *         type: string
 *       value:
 *         type: object
 */

export const settingSchema = new SettingSchema({
  collection: 'Settings',
  toJSON : {virtuals: true},
  toObject : {virtuals: true},
});

settingSchema.virtual('fieldDetails', {
  foreignField: 'setting',
  justOne: true,
  localField: 'originalId',
  ref: 'Field',
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
settingSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Indicies
 */
settingSchema.index({ name: 1 }, { unique: true });

/**
 * toObject
 */
settingSchema.set('toObject', {
  transform: (doc: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
});

settingSchema.set('toJSON', {
  transform: (doc: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
});

/**
 * Methods
 */
settingSchema.method({

});

/**
 * Statics
 */
settingSchema.statics = {

};

/**
 * @typedef Setting
 */
export const settingModel: mongoose.Model<ISettingModel> = mongoose.model<ISettingModel>
  ('Setting', settingSchema, 'Settings', true);
