import * as mongoose from 'mongoose';
import { DuplicateError } from '../../../libs/errors/data';
import { Nullable } from '../../../libs/Nullable';
import VersioningRepository from '../../versionable/VersioningRepository';
import logger from './../../../config/winston';
import { IQueryCreate, IQueryGet, IQueryList, IQueryUpdate } from './entities';
import { environmentModel } from './environmentModel';
import IEnvironmentModel from './IEnvironmentModel';

export default class EnvironmentRepository extends VersioningRepository
<IEnvironmentModel, mongoose.Model<IEnvironmentModel>> {

  constructor() {
    super(environmentModel);
  }

  /**
   * Get environment list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {Environment[]}
   */
  public async list(query: IQueryList): Promise<IEnvironmentModel[]> {

    logger.debug('EnvironmentRepository - List');

    return super.list(query);
  }

  /**
   * Get environment.
   * @property {string} id - _id of the record
   * @returns {Environment}
   */
  public async get(query: IQueryGet): Promise<Nullable<IEnvironmentModel>> {

    logger.debug('EnvironmentRepository - Get');

    return super.get(query);
  }

  /**
   * Create new environment
   * @property {string} body.name - The name of record.
   * @returns {Environment}
   */
  public async create(options: IQueryCreate): Promise<IEnvironmentModel> {

    logger.debug('EnvironmentRepository - Create');

    try {

      await this.checkForDuplicates(options);

      return super.create(options);

    } catch (e) {

      console.log(e);
      throw e;
    }

  }

  /**
   * Create new environment
   * @property {string} id - Record unique identifier.
   * @returns {Environment}
   */
  public async update(options: IQueryUpdate): Promise<IEnvironmentModel> {

    logger.debug('EnvironmentRepository - Update');

    try {

      await this.checkForDuplicates(options);
      return super.update(options);

    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  private async checkForDuplicates(options: any) {

    logger.debug('Checking for duplicates...');

    const duplicatesByName = await this.list({ name: options.name, originalId: { $nin: [options.originalId]} });

    console.log(duplicatesByName);

    if (duplicatesByName && duplicatesByName.length > 0) {

      throw new DuplicateError(
        `Name [${options.name}] already exists and cannot be duplicated`,
        'name', options.name);

    }

    const duplicatesByCaption = await this.list({caption: options.caption, originalId: { $nin: [options.originalId]} });

    if (duplicatesByCaption && duplicatesByCaption.length > 0) {

      throw new DuplicateError(
        `Caption [${options.caption}] already exists and cannot be duplicated`,
        'caption', options.caption);

    }

  }

}
