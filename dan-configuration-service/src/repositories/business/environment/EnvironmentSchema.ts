import VersionableSchema from '../../versionable/VersionableSchema';

/**
 * Environment Schema
 */
/**
 * @swagger
 * definitions:
 *   Environment:
 *     properties:
 *       createdAt:
 *         type: date
 *         description: timestamp for the object being created
 *       deletedAt:
 *         type: date
 *         description: timestamp for the object being invalidated by a delete or update action
 *       objectId:
 *         type: string
 *         description: the unique identifier of the first instance of the record
 *       name:
 *         type: string
 *         description: the Environment name (tag example DEV, UAT, PROD)
 *       caption:
 *         type: string
 *         description: a caption identifying the displayable name of the Environment
 *                      (example Development, User Acceptance Test, Production)
 */
export default class EnvironmentSchema extends VersionableSchema {

  constructor(options: any, collections: any) {
    const baseSchema = {
      caption: {
        required: true,
        type: String,
      },
      name: {
        required: true,
        type: String,
      },
    };

    super(baseSchema, collections);
  }

}
