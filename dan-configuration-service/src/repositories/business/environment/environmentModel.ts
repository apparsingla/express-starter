import * as mongoose from 'mongoose';

import EnvironmentSchema from './EnvironmentSchema';
import IEnvironmentModel from './IEnvironmentModel';

/**
 * Environment Schema
 */
/**
 * @swagger
 * definitions:
 *   Environment:
 *     properties:
 *       name:
 *         type: string
 *       caption:
 *         type: string
 */

export const environmentSchema = new EnvironmentSchema({
  caption: {
    required: true,
    type: String,
  },
  name: {
    required: true,
    type: String,
  },
}, {
    collection: 'environments',
  });

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
environmentSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Indicies
 */
environmentSchema.index({ name: 1 }, { unique: true });

/**
 * toObject
 */
environmentSchema.set('toObject', {
  transform: (doc: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
});

/**
 * Methods
 */
environmentSchema.method({

});

/**
 * Statics
 */
environmentSchema.statics = {

};

/**
 * @typedef Environment
 */
export const environmentModel: mongoose.Model<IEnvironmentModel> = mongoose.model<IEnvironmentModel>
  ('Environment', environmentSchema, 'Environments', true);
