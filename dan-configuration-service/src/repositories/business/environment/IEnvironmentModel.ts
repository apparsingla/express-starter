import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IEnvironmentModel extends IVersionableDocument {
  id: string;
  name: string;
  caption: string;
}
