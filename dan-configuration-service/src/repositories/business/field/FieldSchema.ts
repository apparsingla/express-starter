import { Schema } from 'mongoose';
import VersionableSchema from '../../versionable/VersionableSchema';

/**
 * Field Schema
 */
/**
 * @swagger
 * definitions:
 *   Field:
 *     properties:
 *       createdAt:
 *         type: date
 *         description: timestamp for the object being created
 *       deletedAt:
 *         type: date
 *         description: timestamp for the object being invalidated by a delete or update action
 *       objectId:
 *         type: string
 *         description: the unique identifier of the first instance of the record
 *       internalName:
 *         type: string
 *         description: the name of the field as expected by the application owning the configuration
 *       path:
 *         type: string
 *         description: dotted notation for the position of the field within the configuration hierarchy
 *                      (example {a:{b:{c:'x'}}} would be represented as 'a.b.c')
 *       setting:
 *         type: ObjectId
 *         description: reference to the name or value couple for the field
 *                      (different fields from different configurations may reference
 *                      the same Setting to enforce a dependency
 *                      (example I change a db connection string and it get the update on several applications at once))
 */
export default class FieldSchema extends VersionableSchema {

  constructor(options: any) {
    const baseSchema = {
      internalName: {
        required: true,
        type: String,
      },
      path: {
        required: true,
        type: String,
      },
      setting: {
        ref: 'Setting',
        required: true,
        type: String,
      },
    };

    super(baseSchema, options);
  }

}
