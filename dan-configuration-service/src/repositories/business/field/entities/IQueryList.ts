import { IQueryBaseList } from '../../../entities';

export default interface IQueryList extends IQueryBaseList {
  limit?: number;
  skip?: number;
  internalName?: string;
  path?: string;
  setting?: string;
  ids?: [string];
}
