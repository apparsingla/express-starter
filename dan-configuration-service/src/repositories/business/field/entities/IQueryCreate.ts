import { IQueryBaseCreate } from '../../../entities';

export default interface ICreate extends IQueryBaseCreate {
  internalName: string;
  path: string;
  setting: string;
}
