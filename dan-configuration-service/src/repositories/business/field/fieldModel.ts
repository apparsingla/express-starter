import * as mongoose from 'mongoose';

import FieldSchema from './FieldSchema';
import IFieldModel from './IFieldModel';

/**
 * Field Schema
 */
/**
 * @swagger
 * definitions:
 *   Field:
 *     properties:
 *       name:
 *         type: string
 *       type:
 *         type: string
 *       value:
 *         type: object
 */

export const fieldSchema = new FieldSchema({
  collection: 'Fields',
  toJSON : {virtuals: true},
  toObject : {virtuals: true},
});

fieldSchema.virtual('settingDetails', {
  foreignField: 'originalId',
  justOne: true,
  localField: 'setting',
  ref: 'Setting',
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
fieldSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Indicies
 */
fieldSchema.index({ name: 1 }, { unique: true });

/**
 * toObject
 */
fieldSchema.set('toObject', {
  transform: (doc: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
});

/**
 * Methods
 */
fieldSchema.method({

});

/**
 * Statics
 */
fieldSchema.statics = {

};

/**
 * @typedef Field
 */
export const fieldModel: mongoose.Model<IFieldModel> = mongoose.model
  <IFieldModel>('Field', fieldSchema, 'Fields', true);
