import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IFieldModel extends IVersionableDocument {
  id: string;
  internalName: string;
  setting: string;
}
