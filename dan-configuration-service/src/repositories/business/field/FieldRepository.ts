import * as mongoose from 'mongoose';
import { DuplicateError } from '../../../libs/errors/data';
import { Nullable } from '../../../libs/Nullable';
import VersioningRepository from '../../versionable/VersioningRepository';
import logger from './../../../config/winston';
import { IQueryCreate, IQueryDelete, IQueryGet, IQueryList, IQueryUpdate } from './entities';
import { fieldModel } from './fieldModel';
import IFieldModel from './IFieldModel';

export default class FieldRepository extends VersioningRepository<IFieldModel, mongoose.Model<IFieldModel>> {

  constructor() {
    super(fieldModel);
  }

  /**
   * Get field list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {Field[]}
   */
  public async list(query: IQueryList): Promise<IFieldModel[]> {

    logger.debug('FieldRepository - List');

    return super.list(query);
  }

  /**
   * Get field.
   * @property {string} id - _id of the record
   * @returns {Field}
   */
  public async get(query: IQueryGet): Promise<Nullable<IFieldModel>> {

    logger.debug('FieldRepository - Get');

    return super.get(query);
  }

  /**
   * Create new field
   * @property {string} name - The name of the field variable.
   * @property {string} type - The type of the field variable.
   * @property {object} value - The value of the field variable.
   * @returns {Field}
   */
  public async create(options: IQueryCreate): Promise<IFieldModel> {

    logger.debug('FieldRepository - Create');

    return super.create(options);

  }

  /**
   * Get fields.
   * @property {string[]} ids - _ids of the record
   * @returns {Field}
   */
  public async getAllByIds(query: string[]): Promise<IFieldModel[]> {

    logger.debug('FieldRepository - Get');

    return super.getByIds(query).populate({
      path: 'setting',
    });
  }

  /**
   * Create new field
   * @property {string} id - Record unique identifier.
   * @property {string} internalName - The name of the field variable.
   * @property {string} path - The type of the field variable.
   * @property {object} setting - The value of the field variable.
   * @returns {Field}
   */
  public async update(options: IQueryUpdate): Promise<IFieldModel> {

    logger.debug('FieldRepository - Update');

    return super.update(options);

  }

  /**
   * Delete setting.
   * @property {string} id - _id of the record
   * @returns {Setting}
   */
  public async delete(query: IQueryDelete): Promise<Nullable<IFieldModel>> {

    logger.debug('FieldRepository - Delete');

    return super.invalidate(query.id);
  }

  // @TODO: validation for no duplication within the same configuration

}
