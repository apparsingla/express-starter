import VersionableSchema from '../../versionable/VersionableSchema';

/**
 * User Schema
 */
/**
 * @swagger
 * definitions:
 *   User:
 *     properties:
 *       name:
 *         type: string
 *         description: the Application name (tag example DEV, UAT, PROD)
 *       email:
 *         type: string
 *         description: a caption identifying the displayable name of the Application
 *       password:
 *         type: string
 *         description: a caption identifying the displayable name of the Application
 *       username:
 *         type: string
 *         description: a caption identifying the displayable name of the Application
 */
export default class UserSchema extends VersionableSchema {

  constructor(options: any, collections: any) {
    const baseSchema = {
      email: {
        required: true,
        type: String,
      },
      name: {
        required: true,
        type: String,
      },
      password: {
        required: true,
        type: String,
      },
      username: {
        required: true,
        type: String,
      },
    };

    super(baseSchema, collections);
  }

}
