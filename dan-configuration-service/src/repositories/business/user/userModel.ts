import * as mongoose from 'mongoose';
import IUserModel from './IUserModel';
import UserSchema from './UserSchema';

/**
 * User Schema
 */
/**
 * @swagger
 * definitions:
 *   User:
 *     properties:
 *       name:
 *         type: string
 *         example: 'Tony Stark'
 *       email:
 *         type: string
 *         example: 'iamironman@avengers.com'
 *       username:
 *         type: string
 *         example: 'iamironman'
 *       password:
 *         type: string
 *         example: 'worldissafe'
 */

const userSchema = new UserSchema({
    email: {
        required: true,
        type: String,
    },
    name: {
        required: true,
        type: String,
    },
    password: {
        required: true,
        type: String,
    },
    username: {
        required: true,
        type: String,
    },
}, {
        collection: 'users',
    });

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
userSchema.pre('save', (next: any) => {
    // this.updateDate = new Date();
    next();
});

/**
 * Indicies
 */
userSchema.index({ name: 1 }, { unique: true });

/**
 * toObject
 */
userSchema.set('toObject', {
    transform: (doc: any, ret: any, options: any) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    },
});

/**
 * Methods
 */
userSchema.method({

});

/**
 * Statics
 */
userSchema.statics = {

};

/**
 * @typedef User
 */
export const userModel: mongoose.Model<IUserModel> = mongoose.model<IUserModel>
    ('User', userSchema, 'Users', true);
