import { IQueryBaseList } from '../../../entities';

export default interface IQueryList extends IQueryBaseList {
  username?: string;
  limit?: number;
  skip?: number;
  name?: string;
  email?: string;
}
