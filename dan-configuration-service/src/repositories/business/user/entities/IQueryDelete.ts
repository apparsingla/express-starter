import { IQueryBaseDelete } from '../../../entities';
// import IQueryBaseDelete from '../../../entities/IQueryBaseDelete';

export default interface IQueryDelete extends IQueryBaseDelete {
    id: string;
}
