import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IUserModel extends IVersionableDocument {
  email: string;
  id: string;
  name: string;
  password: string;
  username: string;
}
