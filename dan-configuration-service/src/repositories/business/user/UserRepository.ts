import * as mongoose from 'mongoose';
import { DuplicateError } from '../../../libs/errors/data';
import { Nullable } from '../../../libs/Nullable';
import VersioningRepository from '../../versionable/VersioningRepository';
import logger from './../../../config/winston';
import { IQueryCreate, IQueryDelete, IQueryGet, IQueryList, IQueryUpdate } from './entities';
import IUserModel from './IUserModel';
import { userModel } from './userModel';

export default class UserRepository extends VersioningRepository<IUserModel,
  mongoose.Model<IUserModel>>  {

  constructor() {
    super(userModel);
  }
  /**
   * Create new user
   * @property {string} body.name - The name of record.
   * @property {string} body.email - The name of record.
   * @property {string} body.password - The name of record.
   * @property {string} body.username - The name of record.
   * @returns {User}
   */
  public async create(options: IQueryCreate): Promise<IUserModel> {

    logger.debug('UserRepository - Create');
    try {

      await this.checkForDuplicates(options);

      return super.create(options);

    } catch (e) {
      console.log(e);
      throw e;
    }
  }
  /**
   * Delete user.
   * @property {string} id - _id of the record
   * @returns {User}
   */
  public async delete(options: IQueryDelete): Promise<Nullable<IUserModel>> {

    logger.debug('UserRepository - Delete');
    return super.delete(options);
  }
  /**
   * Get user.
   * @property {string} id - _id of the record
   * @returns {User}
   */
  public async get(query: IQueryGet): Promise<Nullable<IUserModel>> {

    logger.debug('UserRepository - Get');

    return super.get(query);
  }
  /**
   * Get user list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {User[]}
   */
  public async list(query: IQueryList): Promise<IUserModel[]> {

    logger.debug('UserRepository - List query: ', query);

    return super.list(query);
  }
  /**
   * Update user
   * @property {string} id - Record unique identifier.
   * @returns {User}
   */
  public async update(options: IQueryUpdate): Promise<IUserModel> {

    logger.debug('UserRepository - Update');

    try {
      await this.checkForDuplicates(options);

      return super.update(options);

    } catch (e) {

      console.log(e);
      throw e;
    }

  }
  private async checkForDuplicates(options: any) {

    logger.debug('Checking for duplicates...');
    const duplicatesByName = await this.list({ name: options.name, originalId: { $nin: [options.originalId]} });

    console.log(duplicatesByName);

    if (duplicatesByName && duplicatesByName.length > 0) {

      throw new DuplicateError(
        `Name [${options.name}] already exists and cannot be duplicated`,
        'name', options.name);

    }

    const duplicatesByUsername =
    await this.list({ username: options.username, originalId: { $nin: [options.originalId]} });

    if (duplicatesByUsername && duplicatesByUsername.length > 0) {

      throw new DuplicateError(
        `Username [${options.username}] already exists and cannot be duplicated`,
        'username', options.username);

    }
  }

}
