import VersionableSchema from '../../versionable/VersionableSchema';

/**
 * Context Schema
 */
/**
 * @swagger
 * definitions:
 *   Context:
 *     properties:
 *       createdAt:
 *         type: date
 *         description: timestamp for the object being created
 *       deletedAt:
 *         type: date
 *         description: timestamp for the object being invalidated by a delete or update action
 *       objectId:
 *         type: string
 *         description: the unique identifier of the first instance of the record
 *       application:
 *         type: ObjectId
 *         description: reference to the context application
 *                      (a context is an application instance in a specific environment)
 *       environment:
 *         type: ObjectId
 *         description: reference to the context environment
 *                      (a context is an application instance in a specific environment)
 *       url:
 *         type: string - root url for the application in the specific environment
 */
export default class ContextSchema extends VersionableSchema {

  constructor(options: any) {
    const baseSchema = {
      application: {
        ref: 'Application',
        type: String,
      },
      environment: {
        ref: 'Environment',
        type: String,
      },
      url: {
        required: true,
        type: String,
      },
    };

    super(baseSchema, options);
  }

}
