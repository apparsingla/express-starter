import { IQueryBaseCreate } from '../../../entities';

export default interface ICreate extends IQueryBaseCreate {
  url: string;
  application: string;
  environment: string;
}
