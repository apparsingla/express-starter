import { IQueryBaseUpdate } from '../../../entities';

export default interface IQueryUpdate extends IQueryBaseUpdate {
  url?: string;
  application?: string;
  environment?: string;
}
