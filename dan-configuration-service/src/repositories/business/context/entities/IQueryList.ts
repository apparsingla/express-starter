import { IQueryBaseList } from '../../../entities';

export default interface IQueryList extends IQueryBaseList {
  limit?: number;
  skip?: number;
  url?: string;
  application?: string;
  environment?: string;
}
