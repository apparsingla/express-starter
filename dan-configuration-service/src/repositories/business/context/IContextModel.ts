import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IContextModel extends IVersionableDocument {
  id: string;
  url: string;
  application: string;
  environment: string;
}
