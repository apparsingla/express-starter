import * as mongoose from 'mongoose';

import ContextSchema from './ContextSchema';
import IContextModel from './IContextModel';

/**
 * Context Schema
 */
/**
 * @swagger
 * definitions:
 *   Context:
 *     properties:
 *       name:
 *         type: string
 *       caption:
 *         type: string
 */

export const contextSchema = new ContextSchema({
  collection: 'Contexts',
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
contextSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Indicies
 */
contextSchema.index({ name: 1 }, { unique: true });

/**
 * toObject
 */
contextSchema.set('toObject', {
  transform: (doc: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
});

/**
 * Methods
 */
contextSchema.method({

});

/**
 * Statics
 */
contextSchema.statics = {

};

/**
 * @typedef Context
 */
export const contextModel: mongoose.Model<IContextModel> = mongoose.model<IContextModel>
  ('Context', contextSchema, 'Contexts', true);
