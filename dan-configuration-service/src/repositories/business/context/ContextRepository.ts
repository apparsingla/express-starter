import * as mongoose from 'mongoose';
import { BadIdError, DuplicateError } from '../../../libs/errors/data';
import { Nullable } from '../../../libs/Nullable';
import VersioningRepository from '../../versionable/VersioningRepository';
import logger from './../../../config/winston';
import { contextModel } from './contextModel';
import { IQueryCreate, IQueryGet, IQueryList, IQueryUpdate } from './entities';
import IContextModel from './IContextModel';

export default class ContextRepository extends VersioningRepository<IContextModel, mongoose.Model<IContextModel>> {

  constructor() {
    super(contextModel);
  }

  /**
   * Get context list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {Context[]}
   */
  public async list(query: IQueryList): Promise<IContextModel[]> {

    logger.debug('ContextRepository - List', query);

    return super.list(query);
  }

  /**
   * Get context.
   * @property {string} id - _id of the record
   * @returns {Context}
   */
  public async get(query: IQueryGet): Promise<Nullable<IContextModel>> {

    logger.debug('ContextRepository - Get');
/*
    const x = await contextModel.findById(query.id).populate({
      path: 'members',
      //path: 'fields',
      //populate: { path: 'setting' }
  });;

    logger.debug(x);

    //
    return contextModel.findById(query.id).populate({
      path: 'members',
      //path: 'fields',
      //populate: { path: 'setting' }
  });;
  */
    return super.get(query);
  }

  /**
   * Create new context
   * @property {string} url - The base url for the application instance.
   * @property {string} application - The reference to the context application.
   * @property {string} environment - The reference to the context environment.
   * @returns {Context}
   */
  public async create(options: IQueryCreate): Promise<IContextModel> {

    logger.debug('ContextRepository - Create', options);

    try {
      await this.checkForDuplicates(options);

      return super.create(options);

    } catch (e) {

      logger.debug(e);
      throw e;
    }
  }

  /**
   * Create new context
   * @property {string} id - Record unique identifier.
   * @returns {Context}
   */
  public async update(options: IQueryUpdate): Promise<IContextModel> {

    logger.debug('ContextRepository - Update');
    await this.checkForDuplicates(options);

    return super.update(options);

  }

  private async checkForDuplicates(options: any) {

    logger.debug('Checking for duplicates...', { url: options.url, originalId: { $nin: [options.originalId] } });

    const duplicatesByUrl = await this.list({ url: options.url, originalId: { $nin: [options.originalId] } });
    if (duplicatesByUrl && duplicatesByUrl.length > 0) {

      throw new DuplicateError(
        `URL [${options.url}] already exists and cannot be duplicated`, 'url',
        options.url);
    }

    const duplicatesByAppEnv = await this.list({
      application: options.application,
      environment: options.environment,
      originalId: { $nin: [options.originalId] },
    });

    if (duplicatesByAppEnv && duplicatesByAppEnv.length > 0) {

      throw new DuplicateError(
        `The application/environment
        [${options.application}/${options.environment}] already exists and cannot be duplicated`, 'caption/environment',
        `${options.application}/${options.environment}`);

    }
  }

}
