import * as mongoose from 'mongoose';
import { DuplicateError } from '../../../libs/errors/data';
import { Nullable } from '../../../libs/Nullable';
import VersioningRepository from '../../versionable/VersioningRepository';
import logger from './../../../config/winston';
import { applicationModel } from './applicationModel';
import { IQueryCreate, IQueryGet, IQueryList, IQueryUpdate } from './entities';
import IApplicationModel from './IApplicationModel';

export default class ApplicationRepository extends VersioningRepository<IApplicationModel,
  mongoose.Model<IApplicationModel>> {

  constructor() {
    super(applicationModel);
  }

  /**
   * Get application list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {Application[]}
   */
  public async list(query: IQueryList): Promise<IApplicationModel[]> {

    logger.debug('ApplicationRepository - List query: ', query);

    return super.list(query);
  }

  /**
   * Get application.
   * @property {string} id - _id of the record
   * @returns {Application}
   */
  public async get(query: IQueryGet): Promise<Nullable<IApplicationModel>> {

    logger.debug('ApplicationRepository - Get');

    return super.get(query);
  }

  /**
   * Create new application
   * @property {string} body.name - The name of record.
   * @returns {Application}
   */
  public async create(options: IQueryCreate): Promise<IApplicationModel> {

    logger.debug('ApplicationRepository - Create');

    try {

      await this.checkForDuplicates(options);

      return super.create(options);

    } catch (e) {

      console.log(e);
      throw e;
    }
  }

  /**
   * Update application
   * @property {string} id - Record unique identifier.
   * @returns {Application}
   */
  public async update(options: IQueryUpdate): Promise<IApplicationModel> {

    logger.debug('ApplicationRepository - Update');

    try {

      await this.checkForDuplicates(options);

      return super.update(options);

    } catch (e) {

      console.log(e);
      throw e;
    }

  }

  private async checkForDuplicates(options: any) {

    logger.debug('Checking for duplicates...');

    const duplicatesByName = await this.list({name: options.name, originalId: { $nin: [options.originalId]} });

    console.log(duplicatesByName);

    if (duplicatesByName && duplicatesByName.length > 0) {

      throw new DuplicateError(
        `Name [${options.name}] already exists and cannot be duplicated`,
        'name', options.name);

    }

    const duplicatesByCaption = await this.list({caption: options.caption, originalId: { $nin: [options.originalId]} });

    if (duplicatesByCaption && duplicatesByCaption.length > 0) {

      throw new DuplicateError(
        `Caption [${options.caption}] already exists and cannot be duplicated`,
        'caption', options.caption);

    }
  }

}
