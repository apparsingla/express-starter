import IQueryCreate from './IQueryCreate';
import IQueryGet from './IQueryGet';
import IQueryList from './IQueryList';
import IQueryUpdate from './IQueryUpdate';

export{
  IQueryCreate,
  IQueryList,
  IQueryUpdate,
  IQueryGet,
};
