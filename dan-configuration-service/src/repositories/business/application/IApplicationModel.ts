import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IApplicationModel extends IVersionableDocument {
  id: string;
  name: string;
  caption: string;
}
