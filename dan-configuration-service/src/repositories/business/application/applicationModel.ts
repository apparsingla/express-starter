import * as mongoose from 'mongoose';

import ApplicationSchema from './ApplicationSchema';
import IApplicationModel from './IApplicationModel';

/**
 * Application Schema
 */
/**
 * @swagger
 * definitions:
 *   Application:
 *     properties:
 *       name:
 *         type: string
 *       caption:
 *         type: string
 */

export const applicationSchema = new ApplicationSchema({
  caption: {
    required: true,
    type: String,
  },
  name: {
    required: true,
    type: String,
  },
}, {
    collection: 'applications',
  });

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
applicationSchema.pre('save', (next: any) => {
  // this.updateDate = new Date();
  next();
});

/**
 * Indicies
 */
applicationSchema.index({ name: 1 }, { unique: true });

/**
 * toObject
 */
applicationSchema.set('toObject', {
  transform: (doc: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
});

/**
 * Methods
 */
applicationSchema.method({

});

/**
 * Statics
 */
applicationSchema.statics = {

};

/**
 * @typedef Application
 */
export const applicationModel: mongoose.Model<IApplicationModel> = mongoose.model<IApplicationModel>
  ('Application', applicationSchema, 'Applications', true);
