import { IQueryBaseList } from '../../../entities';

export default interface IQueryList extends IQueryBaseList {
  context?: string;
  limit?: number;
  skip?: number;
  application?: string;
  environment?: string;
}
