import { Schema } from 'mongoose';
import VersionableSchema from '../../versionable/VersionableSchema';

/**
 * Configuration Schema
 */

/**
 * @swagger
 * definitions:
 *   Configuration:
 *     properties:
 *       createdAt:
 *         type: date - timestamp for the object being created
 *       deletedAt:
 *         type: date
 *         description: timestamp for the object being invalidated by a delete or update action
 *       objectId:
 *         type: string
 *         description: the unique identifier of the first instance of the record
 *       context:
 *         type: ObjectId
 *         description: a reference to the application/environment the configuration applies to
 *       fields:
 *         type: [ObjectId]
 *         description: references to the fields constituting the body of the configuration
 */
export default class ConfigurationSchema extends VersionableSchema {

  constructor(options: any) {
    const baseSchema = {
      context: {
        required: true,
        type: String,
      },
      fields: {
        required: true,
        type: [{
          ref: 'Field',
          type: String,
        }],
      },
    };

    super(baseSchema, options);
  }
}
