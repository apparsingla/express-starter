import * as mongoose from 'mongoose';
import ConfigurationSchema from './ConfigurationSchema';
import IConfigurationModel from './IConfigurationModel';

/**
 * Configuration Schema
 */
/**
 * @swagger
 * definitions:
 *   Configuration:
 *     properties:
 *       context:
 *         type: string
 *       fields:
 *         type: [string]
 */

export const configurationSchema = new ConfigurationSchema({
  collection: 'Configurations',
  toJSON: {
    transform: (doc, ret) => {
      ret.id = ret._id;
      delete ret._id;
    },
    virtuals: true,
  },
  toObject: { virtuals: true },
});

configurationSchema.virtual('fieldsDetails', {
  foreignField: 'originalId',
  justOne: false,
  localField: 'fields',
  ref: 'Field',
});

configurationSchema.virtual('contextDetails', {
  foreignField: 'originalId',
  justOne: true,
  localField: 'context',
  ref: 'Context',
});

/**
 * Add your
 * - pre-save hook
 * - validation
 * - virtual
 */
configurationSchema.pre('find', (next: any) => {
  // this.deletedAt = null;
  next();
});

configurationSchema.pre('findOne', (next: any) => {
  // this.deletedAt = null;
  next();
});

/**
 * Indicies
 */
configurationSchema.index({ name: 1 }, { unique: true });

/**
 * toObject
 */
configurationSchema.set('toObject', {
  transform: (doc: any, ret: any, options: any) => {
    ret.id = ret._id;
    delete ret._id;
    delete ret.__v;
  },
});

// configurationSchema.set('toJSON', {
//   transform: (doc: any, ret: any, options: any) => {
//      ret.id = ret._id;
//      delete ret._id;
//      delete ret.__v;
//   },
// });

/**
 * Methods
 */
configurationSchema.method({

});

/**
 * Statics
 */
configurationSchema.statics = {

};

/**
 * @typedef Configuration
 */
export const configurationModel: mongoose.Model<IConfigurationModel> =
  mongoose.model<IConfigurationModel>('Configuration', configurationSchema, 'Configurations', true);
