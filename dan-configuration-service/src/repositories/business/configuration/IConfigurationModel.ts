import IVersionableDocument from '../../versionable/IVersionableDocument';

export default interface IConfigurationModel extends IVersionableDocument {
  id: string;
  context: string;
  fields: any[];
}
