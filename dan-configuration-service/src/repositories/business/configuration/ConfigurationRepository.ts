import * as mongoose from 'mongoose';
import { DuplicateError } from '../../../libs/errors/data';
import { Nullable } from '../../../libs/Nullable';
import VersioningRepository from '../../versionable/VersioningRepository';
import logger from './../../../config/winston';
import { configurationModel } from './configurationModel';
import { IQueryCreate, IQueryGet, IQueryList, IQueryUpdate } from './entities';
import IConfigurationModel from './IConfigurationModel';

export default class ConfigurationRepository
extends VersioningRepository<IConfigurationModel, mongoose.Model<IConfigurationModel>> {

  constructor() {
    super(configurationModel);
  }

  /**
   * Get configuration list.
   * @property {number} skip - Number of records to be skipped.
   * @property {number} limit - Limit number of records to be returned.
   * @returns {Configuration[]}
   */
  public async list(query: IQueryList): Promise<IConfigurationModel[]> {

    logger.debug('ConfigurationRepository - List', query);

    const { skip, limit } = query;
    delete query.limit;
    delete query.skip;
    return this.getAll(query, { skip, limit, sort: { createdAt: -1 } }).populate('contextDetails').populate({
      match: { deletedAt: undefined},
      path: 'fieldsDetails',
      populate: {
        match: { deletedAt: undefined},
        path: 'settingDetails',
      },
    });
  }

  /**
   * Get configuration.
   * @property {string} id - _id of the record
   * @returns {Configuration}
   */
  public async get(query: IQueryGet): Promise<Nullable<IConfigurationModel>> {

    logger.debug('ConfigurationRepository - Get', query);

    return super.getById(query.id).populate('contextDetails').populate({
      match: { deletedAt: undefined},
      path: 'fieldsDetails',
      populate: {
        match: { deletedAt: undefined},
        path: 'settingDetails',
      },
    });
  }

  /**
   * Get configurations by query
   * @property {object} query - Query by which user wants to find the config
   * @returns {Configuration}
   */
  public async getConfigByQuery(query: any): Promise<Nullable<IConfigurationModel>> {
    logger.debug('ConfigurationRepository - Get By Query');

    return super.getByQuery(query).populate('contextDetails').populate({
      match: { deletedAt: undefined},
      path: 'fieldsDetails',
      populate: {
        match: { deletedAt: undefined},
        path: 'settingDetails',
      },
    });
  }

  /**
   * Create new configuration
   * @property {string} name - The name of the configuration variable.
   * @property {string} type - The type of the configuration variable.
   * @property {object} value - The value of the configuration variable.
   * @returns {Configuration}
   */
  public async create(options: IQueryCreate): Promise<IConfigurationModel> {

    logger.debug('ConfigurationRepository - Create');

    return super.create(options);

  }

  /**
   * Create new configuration
   * @property {string} id - Record unique identifier.
   * @property {string} name - The name of the configuration variable.
   * @property {string} type - The type of the configuration variable.
   * @property {object} value - The value of the configuration variable.
   * @returns {Configuration}
   */
  public async update(options: IQueryUpdate): Promise<IConfigurationModel> {

    logger.debug('ConfigurationRepository - Update');

    return super.update(options);

  }

}
