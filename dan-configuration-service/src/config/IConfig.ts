import { ISwaggerDefinition } from '../libs/Swagger';
import { IAuthProviderConfig } from '../types';

export interface IConfig extends ISwaggerDefinition {
  env: string;
  port: string;
  mongooseDebug: boolean;
  mongo: string;
  corsOrigin: string;
  swaggerUrl: string;
  apiPrefix: string;
  authProvider: IAuthProviderConfig;
}
