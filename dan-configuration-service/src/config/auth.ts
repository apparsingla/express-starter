// @TODO: will replaced by dynamic config manager

import { IAuthProviderConfig } from '../types';
// TODO replace this with config manager
const local: IAuthProviderConfig = {
  active: true,
  authentication: {
    clientId: 'api://default',
    issuer: 'https://dentsuaegis-test.okta-emea.com/oauth2/default',
  },
  authorization: {
    allRolesEndpoint: 'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/roles',
    allowWhenNoRule: true,
    application: 'config-manager',
    authorizationEndpoint:
      'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/user-roles',
    getAuthorizations: true,
    openDomain: true,
    rules: [
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/applications',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/configurations',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/contexts',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/environments',
      },
    ],
  },
};

const dev: IAuthProviderConfig = {
  active: true,
  authentication: {
    clientId: 'api://default',
    issuer: 'https://dentsuaegis-test.okta-emea.com/oauth2/default',
  },
  authorization: {
    allRolesEndpoint: 'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/roles',
    allowWhenNoRule: true,
    application: 'config-manager',
    authorizationEndpoint:
      'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/user-roles',
    getAuthorizations: true,
    openDomain: true,
    rules: [
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/applications',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/configurations',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/contexts',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/environments',
      },
    ],
  },
};

const uat: IAuthProviderConfig = {
  active: true,
  authentication: {
    clientId: 'api://default',
    issuer: 'https://dentsuaegis-test.okta-emea.com/oauth2/default',
  },
  authorization: {
    allRolesEndpoint: 'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/roles',
    allowWhenNoRule: true,
    application: 'config-manager',
    authorizationEndpoint:
      'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/user-roles',
    getAuthorizations: true,
    openDomain: true,
    rules: [
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/applications',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/configurations',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/contexts',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/environments',
      },
    ],
  },
};

const test: IAuthProviderConfig = {
  active: true,
  authentication: {
    clientId: 'api://default',
    issuer: 'https://dentsuaegis-test.okta-emea.com/oauth2/default',
  },
  authorization: {
    allRolesEndpoint: 'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/roles',
    allowWhenNoRule: true,
    application: 'config-manager',
    authorizationEndpoint:
      'http://tvstack01-dev-kong-kong-proxy.tvstack01-dev.svc.cluster.local:8001/rolemanager/api/user-roles',
    getAuthorizations: true,
    openDomain: true,
    rules: [
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/applications',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/configurations',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/contexts',
      },
      {
        allow: 'true',
        methods: ['POST', 'GET', 'PUT', 'DELETE'],
        route: '/api/environments',
      },
    ],
  },
};

const getAuthProvider: any = () => {
  switch (process.env.NODE_ENV) {
    case 'dev':
      return dev;
    case 'uat':
      return uat;
    case 'test':
      return test;
    default:
      return local;
  }
};

export const authProvider: IAuthProviderConfig = getAuthProvider();
