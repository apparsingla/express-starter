// TODO need to replace with logger service call
// Need to define format
class Logger {
  public static debug = (message: string): void => {
    console.debug(message);
  }

  public static error = (message: string): void => {
    console.error(message);
  }

  public static info = (message: string): void => {
    console.info(message);
  }

  public static log = (message: string): void => {
    console.log(message);
  }
}

export default Logger;
