import { config } from 'dotenv';
import { version } from 'pjson';
import * as constants from '../libs/constants';
import { authProvider } from './auth';
import { IConfig } from './IConfig';
const envVars: NodeJS.ProcessEnv = process.env;

config();

// TODO will replace by dynamic config manager
let mongo: string = envVars.MONGO_URL;
if (envVars.NODE_ENV === constants.EnvVars.TEST) {
  mongo = envVars.MONGO_URL_TEST || `${envVars.MONGO_URL}_TEST`;
}

const isMongooseDebug = (envVars.NODE_ENV === constants.EnvVars.DEV)
  ? true : false;
const configurations = Object.freeze({
  apiPrefix: constants.API_PREFIX,
  authProvider,
  corsOrigin: envVars.CORS_ORIGIN,
  env: envVars.NODE_ENV,
  mongo,
  mongooseDebug: isMongooseDebug,
  port: envVars.PORT,
  swaggerDefinition: {
    basePath: constants.API_PREFIX,
    info: {
      ...constants.ABOUT,
      version,
    },
  },
  swaggerUrl: constants.SWAGGER_URL,
}) as IConfig;

export default configurations;
