import { Router } from 'express';
import * as appInfo from 'pjson';
import logger from './config/winston';
import applicationRouter from './middlewares/application/route';
import configurationRouter from './middlewares/configuration/route';
import contextRouter from './middlewares/context/route';
import environmentRouter from './middlewares/environment/route';
import userRouter from './middlewares/user/route';
const router = Router();

/**
 * @swagger
 * /health-check:
 *   get:
 *     tags:
 *       - General
 *     description: Health Check
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: I am OK
 */
router.get('/health-check', (req, res) => {
  logger.error('Hello');
  res.send('I am OK');
});

/**
 * @swagger
 * /version:
 *   get:
 *     tags:
 *       - General
 *     description: Get Version
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Version Response
 *         schema:
 *           type: object
 *           properties:
 *             version:
 *               type: string
 *               description: Version of the API.
 *             description:
 *               type: string
 *               description: Description of the API.
 *             name:
 *               type: string
 *               description: Name of the API.
 */
router.get('/version', (req, res) => {
  const { version, name, description } = appInfo;
  console.log(`version = ${version}, name = ${name}, description = ${description}`);
  if (!(typeof version && version)) {
    console.error('An error occurred while trying to get version: Version not defined');
    res.status(400).send(new Error('Version not defined'));
  }
  res.json({
    description,
    name,
    version,
  });
});
router.use('/users', userRouter);
router.use('/applications', applicationRouter);
router.use('/environments', environmentRouter);
router.use('/contexts', contextRouter);
router.use('/configurations', configurationRouter);

export default router;
