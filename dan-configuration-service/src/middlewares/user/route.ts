import { Router } from 'express';
import { checkSchema } from 'express-validator/check';
import controllerHandler from '../../libs/controllerHandler';
import UserMiddleware from './UserMiddleware';
import validation from './validation';

const userMiddleware = UserMiddleware.getInstance();
const router = Router();

/**
 * @swagger
 * /users:
 *   post:
 *     tags:
 *       - User
 *     description: Creates a new User
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: name
 *         description: User's name. No invalid characters allowed (&'"?)
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         description: Successfully created
 *         schema:
 *           $ref: '#/definitions/User'
 */

router.route('/')
  .post(
    checkSchema(validation.create as any),
    controllerHandler(userMiddleware.create, validation.create as any),
  )
  /**
   * @swagger
   * /users:
   *   get:
   *     tags:
   *       - User
   *     description: Returns all User objects
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: list
   *         description: List is the number of records you want to fetch
   *         in: query
   *         required: false
   *       - name: skip
   *         description: Skip is the number of records you want to skip
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: An array of Users
   *         schema:
   *           $ref: '#/definitions/User'
   */
  .get(
    checkSchema(validation.list as any),
    controllerHandler(userMiddleware.list, validation.list as any),
  )
  /**
   * @swagger
   * /users:
   *   put:
   *     tags:
   *       - User
   *     description: Returns the updated User
   *     parameters:
   *       - name: body
   *         description: Updated User's name
   *         in: body
   *         required: true
   *         schema:
   *           properties:
   *             id:
   *               type: string
   *             name:
   *               type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Successfully Updated
   *         schema:
   *           $ref: '#/definitions/User'
   */
  .put(
    checkSchema(validation.put as any),
    controllerHandler(userMiddleware.update, validation.put as any),
  );
  /**
   * @swagger
   * /users/{id}:
   *   get:
   *     tags:
   *       - User
   *     description: Returns the User
   *     parameters:
   *       - name: id
   *         description: Unique identifier (mongoId) whose details you want to fetch
   *         in: path
   *         required: true
   *         example: '123456789012345678909876'
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: An Application
   *         schema:
   *           $ref: '#/definitions/User'
   */
router.route('/:id')
.get(
  checkSchema(validation.get as any),
  controllerHandler(userMiddleware.get, validation.get as any),

)
/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     tags:
 *       - User
 *     description: Delete a particular user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Unique identifier (mongoId) whose details you want to delete
 *         in: path
 *         required: true
 *     responses:
 *       200:
 *         description: The User is deleted Successfully.
 *         schema:
 *           $ref: '#/definitions/User'
 */
.delete(
  checkSchema(validation.delete as any),
  controllerHandler(userMiddleware.delete, validation.delete as any),
);

export default router;
