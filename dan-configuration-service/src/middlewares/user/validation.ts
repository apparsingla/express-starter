import BaseRepository from '../../repositories/BaseRepository';

export default Object.freeze({
  // POST /api/users
  create: {
    email: {
      custom: {
        options: (email: string) => {
          return BaseRepository.checkEmail(email);
        },
      },
      errorMessage: 'email is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
      isLength: {
        errorMessage: 'email should be at least 1 chars long',
        // Multiple options would be expressed as an array
        options: { min: 1 },
      },
    },
    name: {
      custom: {
        options: (name: string) => {
          return BaseRepository.checkAlphanum(name);
        },
      },
      errorMessage: 'Name is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
      isLength: {
        errorMessage: 'Name should be at least 1 chars long',
        // Multiple options would be expressed as an array
        options: { min: 1 },
      },
    },
    password: {
      custom: {
        options: (password: string) => {
          return BaseRepository.checkAlphanum(password);
        },
      },
      errorMessage: 'Password is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
      isLength: {
        errorMessage: 'Password should be at least 8 chars long',
        // Multiple options would be expressed as an array
        options: { min: 8 },
      },
    },
    username: {
      custom: {
        options: (username: string) => {
          return BaseRepository.checkAlphanum(username);
        },
      },
      errorMessage: 'Username is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
      isLength: {
        errorMessage: 'Username should be at least 1 chars long',
        // Multiple options would be expressed as an array
        options: { min: 1 },
      },
    },
  },
  // DELETE /api/users/:id
  delete: {
    id: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'ID Bad Format',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['params'],
    },
  },
  // GET /api/users/:id
  get: {
    id: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'ID Bad Format',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['params'],
    },
  },
  // GET /api/users
  list: {
    limit: {
      errorMessage: 'limit is wrong',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['query'],
      isInt: true,
      optional: true,
      toInt: true,
    },
    skip: {
      errorMessage: 'skip count is wrong',
      in: ['query'],
      isInt: true,
      optional: true,
      toInt: true,
    },
  },
  // PUT /api/users
  put: {
    id: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'Id Bad Format',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
    },
    name: {
      custom: {
        options: (name: string) => {
          return BaseRepository.checkAlphanum(name);
        },
      },
      errorMessage: 'Name is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
    },
  },
});
