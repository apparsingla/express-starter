export default interface IList {
  limit: number;
  skip: number;
  name?: string;
  username?: string;
}
