export default interface ICreate {
  email: string;
  name: string;
  password: string;
  username: string;
}
