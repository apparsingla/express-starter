export default interface IUpdate {
  id: string;
  name: string;
}
