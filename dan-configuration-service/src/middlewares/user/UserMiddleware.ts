import logger from '../../config/winston';
import UserController from '../../controllers/UserController';
import IUser from '../../entities/IUser';
import { BadFormatIdError, BadIdError, DuplicateError } from '../../libs/errors/data';
import IError from '../../libs/errors/IError';
import { Nullable } from '../../libs/Nullable';
import SystemResponse from '../../libs/SystemResponse';
import BaseRepository from '../../repositories/BaseRepository';
import { ICreate, IGet, IList, IUpdate } from './entities';

export default class UserMiddleware {
  public static getInstance(): UserMiddleware {
    if (!UserMiddleware.instance) {
      UserMiddleware.instance = new UserMiddleware();
      UserMiddleware.userController = UserController.getInstance();
    }
    return UserMiddleware.instance;
  }
  private static instance: UserMiddleware;
  private static userController: UserController;

  private constructor() { }

  public async delete({ params: { id } }: { params: IGet }): Promise<Nullable<IUser>> {
    const promise: Promise<Nullable<IUser>> = new Promise(
      (resolve: any, reject: any) => {
        const idIsValid = BaseRepository.checkId(id);

        if (idIsValid) {
          UserMiddleware.userController
            .delete(id)
            .then((user: IUser | null) => {
              if (user) {
                resolve(user);
              } else {
                const error = new BadIdError(
                  `User id [${id}] does not exist`,
                  'id',
                  id,
                );
                resolve(SystemResponse.badRequestError([error]));
              }
            })
            .catch((error: IError) => {
              resolve(SystemResponse.serverError([error]));
            });
        } else {
            const error = new BadFormatIdError(
              'id',
              id,
              `User id [${id}] is not in the right format`,
            );
            resolve(SystemResponse.badRequestError([error]));
        }
      },
    );

    return promise;
  }
  public async get({ params: { id } }: { params: IGet }): Promise<Nullable<IUser>> {

    logger.debug('UserMiddleware - Get');
    logger.info(`Getting user [${id}]...`);

    const promise: Promise<Nullable<IUser>> = new Promise(
      (resolve: any, reject: any) => {
        const idIsValid = BaseRepository.checkId(id);

        if (idIsValid) {
          UserMiddleware.userController
            .get(id)
            .then((user: IUser | null) => {
              if (user) {
                resolve(user);
              } else {
                const error = new BadIdError(
                  `User id [${id}] does not exist`,
                  'id',
                  id,
                );
                resolve(SystemResponse.badRequestError([error]));
              }
            })
            .catch((error: IError) => {
              resolve(SystemResponse.serverError([error]));
            });
        } else {
            const error = new BadFormatIdError(
              'id',
              id,
              `User id [${id}] is not in the right format`,
            );
            resolve(SystemResponse.badRequestError([error]));
        }
      },
    );

    return promise;
  }
  public create({ body: { email, name, password, username } }: { body: ICreate }) {
    logger.debug('UserMiddleware - Create');

    const promise: Promise<IUser> = new Promise((resolve: any, reject: any) => {
      UserMiddleware.userController
      .create(name, email, username, password)
        .then((user: IUser) => {
          resolve(user);
        })
        .catch((error) => {
          resolve(SystemResponse.serverError([error]));
        });
    });

    return promise;
  }
  public list({ query: {limit = 50, skip = 0} }: { query: IList }): Promise<IUser[]> {

    logger.debug('UserMiddleware - List');
    logger.info(`Getting [${limit}] users (skipping [${skip}])...`);
    const promise: Promise<IUser[]> = new Promise((resolve: any, reject: any) => {
      UserMiddleware.userController
        .list(limit, skip)
        .then((users: IUser[]) => {
          resolve(users);
        })
        .catch((error: IError) => {
          resolve(SystemResponse.serverError([error]));
        });
      },
    );

    return promise;
  }
  public async update({ body: { id, name } }: { body: IUpdate }) {

    logger.debug('UserMiddleware - Update');
    logger.info(`Updating user [${id}] with name [${name}]...`);

    const promise: Promise<IUser> = new Promise((resolve: any, reject: any) => {

      const idIsValid = BaseRepository.checkId(id);

      if (idIsValid) {

        UserMiddleware.userController
          .update(id, name)
          .then((user: IUser) => {
            resolve(user);
          })
          .catch((error: IError) => {
            if (error.type === DuplicateError.ERROR_TYPE) {
              resolve(SystemResponse.badRequestError([error]));
            } else {
              resolve(SystemResponse.serverError([error]));
            }
          });
      } else {
          const error = new BadFormatIdError(
            'id',
            id,
            `User id [${id}] is not in the right format`,
          );
          resolve(SystemResponse.badRequestError([error]));
      }
    });

    return promise;
  }
}
