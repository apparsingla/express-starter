import ConfigurationController from '../../controllers/ConfigurationController';
import IConfiguration from '../../entities/IConfiguration';
import { BadFormatIdError, BadIdError, DuplicateError } from '../../libs/errors/data';
import IError from '../../libs/errors/IError';
import { Nullable } from '../../libs/Nullable';
import SystemResponse from '../../libs/SystemResponse';
import BaseRepository from '../../repositories/BaseRepository';
import logger from './../../config/winston';
import { ICreate, IGet, IList, IUpdate } from './entities';

export default class ConfigurationMiddleware {

  public static getInstance(): ConfigurationMiddleware {
    if (!ConfigurationMiddleware.instance) {
      ConfigurationMiddleware.instance = new ConfigurationMiddleware();
      ConfigurationMiddleware.configurationController = ConfigurationController.getInstance();
    }

    return ConfigurationMiddleware.instance;
  }

  private static instance: ConfigurationMiddleware;
  private static configurationController: ConfigurationController;

  private constructor() { }

  public async get({ params: { id } }: { params: IGet }): Promise<Nullable<IConfiguration>> {

    logger.debug('ConfigurationMiddleware - Get');
    logger.info(`Getting configuration [${id}]...`);

    const promise: Promise<Nullable<IConfiguration>> = new Promise(
      (resolve: any, reject: any) => {
        const idIsValid = BaseRepository.checkId(id);

        if (idIsValid) {
          ConfigurationMiddleware.configurationController
            .get(id)
            .then((configuration: IConfiguration | null) => {
              if (configuration) {
                resolve(configuration);
              } else {
                const error = new BadIdError(
                  `Configuration id [${id}] does not exist`,
                  'id',
                  id,
                );
                resolve(SystemResponse.badRequestError([error]));
              }
            })
            .catch((error: IError) => {
              resolve(SystemResponse.serverError([error]));
            });
        } else {
          const error = new BadFormatIdError(
            'id',
            id,
            `Configuration id [${id}] is not in the right format`,
          );
          resolve(SystemResponse.badRequestError([error]));
        }
      },
    );

    return promise;
  }

  public async list({ query: { limit = 50, skip = 0, application, environment } }: { query: IList })
    : Promise<IConfiguration[]> {

    logger.debug('ConfigurationMiddleware - List');
    logger.info(`Getting [${limit}] configurations (skipping [${skip}]) for application
    [${application}] environment [${environment}]...`);

    const promise: Promise<IConfiguration[]> = new Promise((resolve: any, reject: any) => {
      ConfigurationMiddleware.configurationController
        .list(limit, skip, application, environment)
        .then((configurations: IConfiguration[]) => {
          resolve(configurations);
        })
        .catch((error: IError) => {
          resolve(SystemResponse.serverError([error]));
        });
    },
    );

    return promise;
  }

  public async create({ body: { context, configurationBody } }: { body: ICreate }) {

    logger.debug('ConfigurationMiddleware - Create');
    logger.info(`Creating configuration for context name [${context}]...`);
    const promise: Promise<IConfiguration> = new Promise((resolve: any, reject: any) => {
      ConfigurationMiddleware.configurationController
        .create(context, configurationBody)
        .then((configuration: IConfiguration) => {
          resolve(configuration);
        })
        .catch((error: IError) => {
          if (error.type === DuplicateError.ERROR_TYPE) {
            resolve(SystemResponse.badRequestError([error]));
          } else {
            resolve(SystemResponse.serverError([error]));
          }
        });
    });

    return promise;
  }

  public async update({ body: { id, configurationBody } }: { body: IUpdate }) {

    logger.debug('ConfigurationMiddleware - Update');
    logger.info(`Updating configuration for context [${id}]...`);

    const promise: Promise<IConfiguration> = new Promise((resolve: any, reject: any) => {

      const idIsValid = BaseRepository.checkId(id);
      logger.debug(configurationBody);
      if (idIsValid) {

        ConfigurationMiddleware.configurationController
          .update(id, configurationBody)
          .then((configuration: IConfiguration) => {
            resolve(configuration);
          })
          .catch((error: IError) => {
            if (error.type === DuplicateError.ERROR_TYPE) {
              resolve(SystemResponse.badRequestError([error]));
            } else {
              resolve(SystemResponse.serverError([error]));
            }
          });
      } else {
        const error = new BadFormatIdError(
          'id',
          id,
          `Configuration id [${id}] is not in the right format`,
        );
        resolve(SystemResponse.badRequestError([error]));
      }
    });

    return promise;
  }
}
