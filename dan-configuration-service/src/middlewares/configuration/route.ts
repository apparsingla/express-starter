// import { AuthManager } from 'dan-auth-middleware';
import { Router } from 'express';
import { checkSchema } from 'express-validator/check';
import controllerHandler from '../../libs/controllerHandler';
import ConfigurationMiddleware from './ConfigurationMiddleware';
import validation from './validation';

// const authManager: AuthManager = AuthManager.getInstance();
const configurationMiddleware = ConfigurationMiddleware.getInstance();
const router = Router();

/**
 * @swagger
 * /configurations:
 *   post:
 *     tags:
 *       - Configuration
 *     description: Creates a new Configuration
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: context
 *         description: Unique identifer (mongoId) of the context
 *         in: body
 *         required: true
 *         example: '123456789012345678909876'
 *       - name: configurationBody
 *         description: Configurations for the context
 *         in: body
 *         required: true
 *         example: {
 *              name: Configuration Service,
 *              version: 1,
 *              working: true,
 *           }
 *     responses:
 *       200:
 *         description: Successfully created
 *         schema:
 *           $ref: '#/definitions/Configuration'
 */
router.route('/')
  .post(
    // authManager.auth,
    checkSchema(validation.create as any),
    controllerHandler(configurationMiddleware.create, validation.create as any),
)

  /**
   * @swagger
   * /configurations:
   *   get:
   *     tags:
   *       - Configuration
   *     description: Returns all Configuration objects
   *     parameters:
   *       - name: list
   *         description: List is the number of records you want to fetch
   *         in: query
   *         required: false
   *       - name: skip
   *         description: Skip is the number of records you want to skip
   *         in: query
   *         required: false
   *       - name: application
   *         description: application originalId which should be hexadecimal string existing in database
   *         in: query
   *         required: false
   *       - name: environment
   *         description: environment originalId which should be hexadecimal string existing in database
   *         in: query
   *         required: false
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: An array of Configurations
   *         schema:
   *           $ref: '#/definitions/Configuration'
   */
  .get(
    // authManager.auth,
    checkSchema(validation.list as any),
    controllerHandler(configurationMiddleware.list, validation.list as any),
)

/**
 * @swagger
 * /configurations:
 *   put:
 *     tags:
 *       - Configuration
 *     description: Updates the existing Configuration
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Unique identifier (mongoId) of the configuration
 *         in: body
 *         required: true
 *         example: '123456789012345678909876'
 *       - name: configurationBody
 *         description: Updated configurations and new configurations for the associated context
 *         in: body
 *         required: true
 *         example: {
 *              name: Configuration Service,
 *              version: 1,
 *              working: true,
 *           }
 *     responses:
 *       200:
 *         description: Successfully updated
 *         schema:
 *           $ref: '#/definitions/Configuration'
 */
  .put(
    // authManager.auth,
    checkSchema(validation.put as any),
    controllerHandler(configurationMiddleware.update, validation.put as any),
);

  /**
   * @swagger
   * /configurations/:id:
   *   get:
   *     tags:
   *       - Configuration
   *     description: Returns an Configuration object
   *     parameters:
   *       - name: id
   *         description: Unique identifier (mongoId) whose details you want to fetch
   *         in: query
   *         required: true
   *         example: '123456789012345678909876'
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: An Configuration
   *         schema:
   *           $ref: '#/definitions/Configuration'
   */
router.route('/:id')
  .get(
    // authManager.auth,
    checkSchema(validation.get as any),
    controllerHandler(configurationMiddleware.get, validation.get as any),
    );

export default router;
