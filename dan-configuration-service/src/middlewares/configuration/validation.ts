import BaseRepository from '../../repositories/BaseRepository';

export default Object.freeze({
  // POST /api/configurations
  create: {
    configurationBody: {
      custom: {
        options: (configurationBody: object) => {
          return BaseRepository.checkForValidJSON(configurationBody);
        },
      },
      errorMessage: 'Configuration Body is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
      isLength: {
        errorMessage: 'configurationBody should contain atleast 1 configuration',
        // Multiple options would be expressed as an array
        options: { min: 1 },
      },
    },
    context: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'ID Bad Format',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
    },
  },
  // GET /api/configurations/:id
  get: {
    id: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'ID Bad Format',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['params'],
    },
  },
  // GET /api/configurations
  list: {
    application: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'Application is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['query'],
      optional: true,
    },
    environment: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'Environment is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['query'],
      optional: true,
    },
    limit: {
      errorMessage: 'limit is wrong',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['query'],
      isInt: true,
      optional: true,
      toInt: true,
    },
    skip: {
      errorMessage: 'skip count is wrong',
      in: ['query'],
      isInt: true,
      optional: true,
      toInt: true,
    },
  },
  // PUT /api/configurations
  put: {
    configurationBody: {
      custom: {
        options: (configurationBody: object) => {
          return BaseRepository.checkForValidJSON(configurationBody);
        },
      },
      errorMessage: 'Configuration Body is wrong!',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
      isLength: {
        errorMessage: 'configurationBody should contain atleast 1 configuration',
        // Multiple options would be expressed as an array
        options: { min: 1 },
      },
    },
    id: {
      custom: {
        options: (id: string) => {
          return BaseRepository.checkId(id);
        },
      },
      errorMessage: 'ID Bad Format',
      // The location of the field, can be one or more of body, cookies, headers, params or query.
      // If omitted, all request locations will be checked
      in: ['body'],
    },
  },
});
