import ICreate from './ICreate';
import IGet from './IGet';
import IList from './IList';
import IUpdate from './IUpdate';

export{
  ICreate,
  IGet,
  IList,
  IUpdate,
};
