export default interface IUpdate {
  id: string;
  configurationBody: any;
}
