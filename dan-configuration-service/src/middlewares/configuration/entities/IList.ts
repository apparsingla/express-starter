export default interface IList {
  limit: number;
  skip: number;
  application?: string;
  environment?: string;
}
