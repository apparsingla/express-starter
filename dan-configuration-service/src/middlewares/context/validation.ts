import BaseRepository from '../../repositories/BaseRepository';

export default Object.freeze({
    // POST /api/contexts
    create: {
      application: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'Application is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
      },
      environment: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'Environment is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
      },
      url: {
        custom: {
          options: (url: string) => {
            return BaseRepository.checkForInvalidURL(url);
          },
        },
        errorMessage: 'Url is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
      },
    },
    // GET /api/contexts/:id
    get: {
      id: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'ID Bad Format',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['params'],
      },
    },
    // GET /api/contexts
    list: {
      limit: {
        errorMessage: 'limit is wrong',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['query'],
        isInt: true,
        optional: true,
        toInt: true,
      },
      skip: {
        errorMessage: 'skip count is wrong',
        in: ['query'],
        isInt: true,
        optional: true,
        toInt: true,
      },
    },
    // PUT /api/contexts
    put: {
      application: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'Application is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
      },
      environment: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'Environment is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
      },
      id: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'Id Bad Format',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
        // isInt: true,
        // Sanitizers can go here as well
        // toInt: true,
      },
      url: {
        custom: {
          options: (url: string) => {
            return BaseRepository.checkForInvalidURL(url);
          },
        },
        errorMessage: 'Url is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
        optional: true,
      },
    },
});
