// import { AuthManager } from 'dan-auth-middleware';
import { Router } from 'express';
import { checkSchema } from 'express-validator/check';
import controllerHandler from '../../libs/controllerHandler';
import ContextMiddleware from './ContextMiddleware';
import validation from './validation';

// const authManager: AuthManager = AuthManager.getInstance();
const contextMiddleware = ContextMiddleware.getInstance();
const router = Router();

/**
 * @swagger
 * /contexts:
 *   post:
 *     tags:
 *       - Context
 *     description: Creates a new Context
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: application
 *         description: Unique identifier (mongoId) of the application
 *         in: body
 *         required: true
 *         example: '123456789012345678909876'
 *       - name: environment
 *         description: Unique identifier (mongoId) of the environment
 *         in: body
 *         required: true
 *         example: '123456789012345678909876'
 *       - name: url
 *         description: URL for the application with its environment
 *         in: body
 *         required: true
 *         example:
 *           url: 'http://localhost:9000'
 *     responses:
 *       200:
 *         description: Successfully created
 *         schema:
 *           $ref: '#/definitions/Context'
 */
router.route('/')
  .post(
    // authManager.auth,
    checkSchema(validation.create as any),
    controllerHandler(contextMiddleware.create, validation.create as any),
)

  /**
   * @swagger
   * /contexts:
   *   get:
   *     tags:
   *       - Context
   *     description: Returns all Context objects
   *     parameters:
   *       - name: list
   *         description: List is the number of records you want to fetch
   *         in: query
   *         required: false
   *       - name: skip
   *         description: Skip is the number of records you want to skip
   *         in: query
   *         required: false
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: An array of Contexts
   *         schema:
   *           $ref: '#/definitions/Context'
   */
  .get(
    // authManager.auth,
    checkSchema(validation.list as any),
    controllerHandler(contextMiddleware.list, validation.list as any),
)

  /**
   * @swagger
   * /contexts:
   *   put:
   *     tags:
   *       - Context
   *     description: Returns the updated Context objects
   *     parameters:
   *       - name: application
   *         description: Unique identifier (mongoId) of the application
   *         in: body
   *         required: true
   *         example: '123456789012345678909876'
   *       - name: environment
   *         description: Unique identifier (mongoId) of the environment
   *         in: body
   *         required: true
   *         example: '123456789012345678909876'
   *       - name: url
   *         description: Updated URL for the application with its environment
   *         in: body
   *         required: true
   *         example: 'http://localhost:9000'
   *       - name: id
   *         description: Unique Identifier (mongoId) of the context
   *         in: body
   *         required: true
   *         example: '123456789012345678909876'
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Successfully Updated
   *         schema:
   *           $ref: '#/definitions/Context'
   */
  .put(
    // authManager.auth,
    checkSchema(validation.put as any),
    controllerHandler(contextMiddleware.update, validation.put as any),
);

/**
 * @swagger
 * /contexts/:id:
 *   get:
 *     tags:
 *       - Context
 *     description: Returns an Context
 *     parameters:
 *       - name: id
 *         description: Unique identifier (mongoId) whose details you want to fetch
 *         in: query
 *         required: true
 *         example: '123456789012345678909876'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A Context
 *         schema:
 *           $ref: '#/definitions/Context'
 */
router.route('/:id')
  .get(
    // authManager.auth,
    checkSchema(validation.get as any),
    controllerHandler(contextMiddleware.get, validation.get as any),

);

export default router;
