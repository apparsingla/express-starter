export default interface ICreate {
  url: string;
  application: string;
  environment: string;
}
