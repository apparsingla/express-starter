export default interface IUpdate {
  id: string;
  url: string;
  application: string;
  environment: string;
}
