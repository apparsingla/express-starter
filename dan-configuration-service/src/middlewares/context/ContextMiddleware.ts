import ContextController from '../../controllers/ContextController';
import IContext from '../../entities/IContext';
import { BadFormatIdError, BadIdError, DuplicateError } from '../../libs/errors/data';
import IError from '../../libs/errors/IError';
import { Nullable } from '../../libs/Nullable';
import SystemResponse from '../../libs/SystemResponse';
import BaseRepository from '../../repositories/BaseRepository';
import logger from './../../config/winston';
import { ICreate, IGet, IList, IUpdate } from './entities';

export default class ContextMiddleware {

  public static getInstance(): ContextMiddleware {
    if (!ContextMiddleware.instance) {
      ContextMiddleware.instance = new ContextMiddleware();
      ContextMiddleware.contextController = ContextController.getInstance();
    }

    return ContextMiddleware.instance;
  }

  private static instance: ContextMiddleware;
  private static contextController: ContextController;

  private constructor() { }

  public async get({ params: { id } }: { params: IGet }): Promise<Nullable<IContext>> {

    logger.debug('ContextMiddleware - Get');
    logger.info(`Getting context [${id}]...`);

    const promise: Promise<Nullable<IContext>> = new Promise(
      (resolve: any, reject: any) => {

        const idIsValid = BaseRepository.checkId(id);

        if (idIsValid) {
          ContextMiddleware.contextController
            .get(id)
            .then((context: IContext | null) => {
              if (context) {
                resolve(context);
              } else {
                const error = new BadIdError(
                  `Context id [${id}] does not exist`,
                  'id',
                  id,
                );
                resolve(SystemResponse.badRequestError([error]));
              }
            })
            .catch((error: IError) => {
              resolve(SystemResponse.serverError([error]));
            });
        } else {
          const error = new BadFormatIdError(
              'id',
              id,
              `Context id [${id}] is not in the right format`,
            );
          resolve(SystemResponse.badRequestError([error]));
        }
      },
    );

    return promise;
  }

  public async list({ query: {limit = 50, skip = 0} }: { query: IList }): Promise<IContext[]> {

    logger.debug('ContextMiddleware - List');
    logger.info(`Getting [${limit}] contexts (skipping [${skip}])...`);

    const promise: Promise<IContext[]> = new Promise((resolve: any, reject: any) => {
      ContextMiddleware.contextController
        .list(limit, skip)
        .then((contexts: IContext[]) => {
          resolve(contexts);
        })
        .catch((error: IError) => {
          resolve(SystemResponse.serverError([error]));
        });
      },
    );

    return promise;
  }

  public async create({ body: { url, application, environment } }: { body: ICreate }) {

    logger.debug('ContextMiddleware - Create');
    logger.info(`Creating context for application [${application}] in environment [${environment}] at url [${url}]...`);

    const promise: Promise<IContext> = new Promise((resolve: any, reject: any) => {
      ContextMiddleware.contextController
        .create(url, application, environment)
        .then((context: IContext) => {
          resolve(context);
        })
        .catch((error: IError) => {
          if (error.type === DuplicateError.ERROR_TYPE) {
            resolve(SystemResponse.badRequestError([error]));
          } else {
            resolve(SystemResponse.serverError([error]));
          }
        });
    });

    return promise;
  }

  public async update({ body: { id, url, application, environment } }: { body: IUpdate }) {

    logger.debug('ContextMiddleware - Update');
    logger.info(
      `Updating context [${id}] with application [${application}], environment [${environment}] and url [${url}]...`,
      );

    const promise: Promise<IContext> = new Promise((resolve: any, reject: any) => {

      const idIsValid = BaseRepository.checkId(id);

      if (idIsValid) {

        ContextMiddleware.contextController
          .update(id, url, application, environment)
          .then((context: IContext) => {
            resolve(context);
          })
          .catch((error: IError) => {
            if (error.type === DuplicateError.ERROR_TYPE) {
              resolve(SystemResponse.badRequestError([error]));
            } else {
              resolve(SystemResponse.serverError([error]));
            }
          });
      } else {
        const error = new BadFormatIdError(
            'id',
            id,
            `Context id [${id}] is not in the right format`,
          );
        resolve(SystemResponse.badRequestError([error]));
      }
    });

    return promise;
  }
}
