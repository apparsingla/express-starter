export default interface IList {
  limit: number;
  skip: number;
}
