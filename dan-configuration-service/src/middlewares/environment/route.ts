// import { AuthManager } from 'dan-auth-middleware';
import { Router } from 'express';
import { checkSchema } from 'express-validator/check';
import controllerHandler from '../../libs/controllerHandler';
import EnvironmentMiddleware from './EnvironmentMiddleware';
import validation from './validation';

// const authManager: AuthManager = AuthManager.getInstance();
const environmentMiddleware = EnvironmentMiddleware.getInstance();
const router = Router();

/**
 * @swagger
 * /environments:
 *   post:
 *     tags:
 *       - Environment
 *     description: Creates a new Environment
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: name
 *         description: Environment name. No invalid characters allowed (&'"?)
 *         in: body
 *         required: true
 *         example: 'Configuration Service'
 *       - name: caption
 *         description: Long description about the Environment, default equals name.
 *                      No special characters allowed (!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?)
 *         in: body
 *         required: false
 *         example: 'Configuration Service for saving configurations of all services at same place'
 *     responses:
 *       200:
 *         description: Successfully created
 *         schema:
 *           $ref: '#/definitions/Environment'
 */
router.route('/')
  .post(
    // authManager.auth,
    checkSchema(validation.create as any),
    controllerHandler(environmentMiddleware.create, validation.create as any),
)

  /**
   * @swagger
   * /environments:
   *   get:
   *     tags:
   *       - Environment
   *     description: Returns all Environment objects
   *     parameters:
   *       - name: list
   *         description: List is the number of records you want to fetch
   *         in: query
   *         required: false
   *       - name: skip
   *         description: Skip is the number of records you want to skip
   *         in: query
   *         required: false
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: An array of Environments
   *         schema:
   *           $ref: '#/definitions/Environment'
   */
  .get(
    // authManager.auth,
    checkSchema(validation.list as any),
    controllerHandler(environmentMiddleware.list, validation.list as any),
)

  /**
   * @swagger
   * /environments:
   *   put:
   *     tags:
   *       - Environment
   *     description: Returns the updated Environment objects
   *     parameters:
   *       - name: name
   *         description: Updated Environment name
   *         in: body
   *         required: false
   *         example: 'Configuration Service'
   *       - name: caption
   *         description: Updated description about the Environment
   *         in: body
   *         required: false
   *         example: 'Configuration Service for saving configurations of all services at same place'
   *       - name: id
   *         description: Unique Identifier (mongoId) of the environment
   *         in: body
   *         required: true
   *         example: '123456789012345678909876'
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Successfully Updated
   *         schema:
   *           $ref: '#/definitions/Environment'
   */
  .put(
    // authManager.auth,
    checkSchema(validation.put as any),
    controllerHandler(environmentMiddleware.update, validation.put as any),
);

/**
 * @swagger
 * /environments/:id:
 *   get:
 *     tags:
 *       - Environment
 *     description: Returns an Environment
 *     parameters:
 *       - name: id
 *         description: Unique identifier (mongoId) whose details you want to fetch
 *         in: query
 *         required: true
 *         example: '123456789012345678909876'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An Environment
 *         schema:
 *           $ref: '#/definitions/Environment'
 */
router.route('/:id')
  .get(
    // authManager.auth,
    checkSchema(validation.get as any),
    controllerHandler(environmentMiddleware.get, validation.get as any),

);

export default router;
