import EnvironmentController from '../../controllers/EnvironmentController';
import IEnvironment from '../../entities/IEnvironment';
import { BadFormatIdError, BadIdError, DuplicateError } from '../../libs/errors/data';
import IError from '../../libs/errors/IError';
import { Nullable } from '../../libs/Nullable';
import SystemResponse from '../../libs/SystemResponse';
import BaseRepository from '../../repositories/BaseRepository';
import logger from './../../config/winston';
import { ICreate, IGet, IList, IUpdate } from './entities';

export default class EnvironmentMiddleware {

  public static getInstance(): EnvironmentMiddleware {
    if (!EnvironmentMiddleware.instance) {
      EnvironmentMiddleware.instance = new EnvironmentMiddleware();
      EnvironmentMiddleware.environmentController = EnvironmentController.getInstance();
    }

    return EnvironmentMiddleware.instance;
  }

  private static instance: EnvironmentMiddleware;
  private static environmentController: EnvironmentController;

  private constructor() { }

  public async get({ params: { id } }: { params: IGet }): Promise<Nullable<IEnvironment>> {

    logger.debug('EnvironmentMiddleware - Get');
    logger.info(`Getting environment [${id}]...`);

    const promise: Promise<Nullable<IEnvironment>> = new Promise(
      (resolve: any, reject: any) => {

        const idIsValid = BaseRepository.checkId(id);

        if (idIsValid) {
          EnvironmentMiddleware.environmentController
            .get(id)
            .then((environment: IEnvironment | null) => {
              if (environment) {
                resolve(environment);
              } else {
                const error = new BadIdError(
                  `Environment id [${id}] does not exist`,
                  'id',
                  id,
                );
                resolve(SystemResponse.badRequestError([error]));
              }
            })
            .catch((error: IError) => {
              resolve(SystemResponse.serverError([error]));
            });
        } else {
          const error = new BadFormatIdError(
              'id',
              id,
              `Environment id [${id}] is not in the right format`,
            );
          resolve(SystemResponse.badRequestError([error]));
        }
      },
    );

    return promise;
  }

  public async list({ query: {limit = 50, skip = 0} }: { query: IList }): Promise<IEnvironment[]> {

    logger.debug('EnvironmentMiddleware - List');
    logger.info(`Getting [${limit}] environments (skipping [${skip}])...`);

    const promise: Promise<IEnvironment[]> = new Promise((resolve: any, reject: any) => {
      EnvironmentMiddleware.environmentController
        .list(limit, skip)
        .then((environments: IEnvironment[]) => {
          resolve(environments);
        })
        .catch((error: IError) => {
          resolve(SystemResponse.serverError([error]));
        });
      },
    );

    return promise;
  }

  public async create({ body: { name, caption } }: { body: ICreate }) {

    logger.debug('EnvironmentMiddleware - Create');
    logger.info(`Creating environment with name [${name}] and caption [${caption}]...`);

    const promise: Promise<IEnvironment> = new Promise((resolve: any, reject: any) => {
      EnvironmentMiddleware.environmentController
        .create(name, caption)
        .then((environment: IEnvironment) => {
          resolve(environment);
        })
        .catch((error: IError) => {
          if (error.type === DuplicateError.ERROR_TYPE) {
            resolve(SystemResponse.badRequestError([error]));
          } else {
            resolve(SystemResponse.serverError([error]));
          }
        });
    });

    return promise;
  }

  public async update({ body: { id, name, caption } }: { body: IUpdate }) {

    logger.debug('EnvironmentMiddleware - Update');
    logger.info(`Updating environment [${id}] with name [${name}] and caption [${caption}]...`);

    const promise: Promise<IEnvironment> = new Promise((resolve: any, reject: any) => {

      const idIsValid = BaseRepository.checkId(id);

      if (idIsValid) {
        EnvironmentMiddleware.environmentController
          .update(id, name, caption)
          .then((environment: IEnvironment) => {
            resolve(environment);
          })
          .catch((error: IError) => {
            if (error.type === DuplicateError.ERROR_TYPE) {
              resolve(SystemResponse.badRequestError([error]));
            } else {
              resolve(SystemResponse.serverError([error]));
            }
          });
      } else {
        const error = new BadFormatIdError(
            'id',
            id,
            `Environment id [${id}] is not in the right format`,
          );
        resolve(SystemResponse.badRequestError([error]));
      }
    });

    return promise;
  }
}
