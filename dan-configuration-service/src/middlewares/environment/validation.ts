import BaseRepository from '../../repositories/BaseRepository';

export default Object.freeze({
    // POST /api/environments
    create: {
      caption: {
        custom: {
          options: (caption: string) => {
            return BaseRepository.checkForInvalidCharacters(caption);
          },
        },
        errorMessage: 'Caption is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
      },
      name: {
        custom: {
          options: (name: string) => {
            return BaseRepository.checkAlphanum(name);
          },
        },
        errorMessage: 'Name is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
        isLength: {
          errorMessage: 'Name should be at least 1 chars long',
          // Multiple options would be expressed as an array
          options: { min: 1 },
        },
      },
    },
    // GET /api/environments/:id
    get: {
      id: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'ID Bad Format',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['params'],
      },
    },
    // GET /api/environments
    list: {
      limit: {
        errorMessage: 'limit is wrong',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['query'],
        isInt: true,
        optional: true,
        toInt: true,
      },
      skip: {
        errorMessage: 'skip count is wrong',
        in: ['query'],
        isInt: true,
        optional: true,
        toInt: true,
      },
    },
    // PUT /api/environments
    put: {
      caption: {
        custom: {
          options: (caption: string) => {
            return BaseRepository.checkForInvalidCharacters(caption);
          },
        },
        errorMessage: 'Caption is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
        optional: true,
      },
      id: {
        custom: {
          options: (id: string) => {
            return BaseRepository.checkId(id);
          },
        },
        errorMessage: 'Id Bad Format',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
        // isInt: true,
        // Sanitizers can go here as well
        // toInt: true,
      },
      name: {
        custom: {
          options: (name: string) => {
            return BaseRepository.checkAlphanum(name);
          },
        },
        errorMessage: 'Name is wrong!',
        // The location of the field, can be one or more of body, cookies, headers, params or query.
        // If omitted, all request locations will be checked
        in: ['body'],
        optional: true,
      },
    },
});
