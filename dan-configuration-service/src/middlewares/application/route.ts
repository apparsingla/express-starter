// import { AuthManager } from 'dan-auth-middleware';
import { Router } from 'express';
import { checkSchema } from 'express-validator/check';
import controllerHandler from '../../libs/controllerHandler';
import ApplicationMiddleware from './ApplicationMiddleware';
import validation from './validation';

// const authManager: AuthManager = AuthManager.getInstance();
const applicationMiddleware = ApplicationMiddleware.getInstance();
const router = Router();

/**
 * @swagger
 * /applications:
 *   post:
 *     tags:
 *       - Application
 *     description: Creates a new Application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: name
 *         description: Application name. No invalid characters allowed (&'"?)
 *         in: body
 *         required: true
 *         example: 'Configuration Service'
 *       - name: caption
 *         description: Long description about the Application, default equals name.
 *                      No special characters allowed (!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?)
 *         in: body
 *         required: false
 *         example: 'Configuration Service for saving configurations of all services at same place'
 *     responses:
 *       200:
 *         description: Successfully created
 *         schema:
 *           $ref: '#/definitions/Application'
 */
router.route('/')
  .post(
    // authManager.auth,
    checkSchema(validation.create as any),
    controllerHandler(applicationMiddleware.create, validation.create as any),
)

  /**
   * @swagger
   * /applications:
   *   get:
   *     tags:
   *       - Application
   *     description: Returns all Application objects
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: list
   *         description: List is the number of records you want to fetch
   *         in: query
   *         required: false
   *       - name: skip
   *         description: Skip is the number of records you want to skip
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: An array of Applications
   *         schema:
   *           $ref: '#/definitions/Application'
   */
  .get(
    // authManager.auth,
    checkSchema(validation.list as any),
    controllerHandler(applicationMiddleware.list, validation.list as any),
)

  /**
   * @swagger
   * /applications:
   *   put:
   *     tags:
   *       - Application
   *     description: Returns the updated Application objects
   *     parameters:
   *       - name: name
   *         description: Updated Application name
   *         in: body
   *         required: false
   *         example: 'Configuration Service'
   *       - name: caption
   *         description: Updated description about the Application
   *         in: body
   *         required: false
   *         example: 'Configuration Service for saving configurations of all services at same place'
   *       - name: id
   *         description: Unique Identifier (mongoId) of the application
   *         in: body
   *         required: true
   *         example: '123456789012345678909876'
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Successfully Updated
   *         schema:
   *           $ref: '#/definitions/Application'
   */
  .put(
    // authManager.auth,
    checkSchema(validation.put as any),
    controllerHandler(applicationMiddleware.update, validation.put as any),
);

/**
 * @swagger
 * /applications/:id:
 *   get:
 *     tags:
 *       - Application
 *     description: Returns an Application
 *     parameters:
 *       - name: id
 *         description: Unique identifier (mongoId) whose details you want to fetch
 *         in: query
 *         required: true
 *         example: '123456789012345678909876'
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An Application
 *         schema:
 *           $ref: '#/definitions/Application'
 */
router.route('/:id')
  .get(
    // authManager.auth,
    checkSchema(validation.get as any),
    controllerHandler(applicationMiddleware.get, validation.get as any),

);

export default router;
