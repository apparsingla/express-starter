export default interface ICreate {
  name: string;
  caption: string;
}
