import ApplicationController from '../../controllers/ApplicationController';
import IApplication from '../../entities/IApplication';
import { BadFormatIdError, BadIdError, DuplicateError } from '../../libs/errors/data';
import IError from '../../libs/errors/IError';
import { Nullable } from '../../libs/Nullable';
import SystemResponse from '../../libs/SystemResponse';
import BaseRepository from '../../repositories/BaseRepository';
import logger from './../../config/winston';
import { ICreate, IGet, IList, IUpdate } from './entities';

export default class ApplicationMiddleware {

  public static getInstance(): ApplicationMiddleware {
    if (!ApplicationMiddleware.instance) {
      ApplicationMiddleware.instance = new ApplicationMiddleware();
      ApplicationMiddleware.applicationController = ApplicationController.getInstance();
    }

    return ApplicationMiddleware.instance;
  }

  private static instance: ApplicationMiddleware;
  private static applicationController: ApplicationController;

  private constructor() { }

  public async get({ params: { id } }: { params: IGet }): Promise<Nullable<IApplication>> {

    logger.debug('ApplicationMiddleware - Get');
    logger.info(`Getting application [${id}]...`);

    const promise: Promise<Nullable<IApplication>> = new Promise(
      (resolve: any, reject: any) => {
        const idIsValid = BaseRepository.checkId(id);

        if (idIsValid) {
          ApplicationMiddleware.applicationController
            .get(id)
            .then((application: IApplication | null) => {
              if (application) {
                resolve(application);
              } else {
                const error = new BadIdError(
                  `Application id [${id}] does not exist`,
                  'id',
                  id,
                );
                resolve(SystemResponse.badRequestError([error]));
              }
            })
            .catch((error: IError) => {
              resolve(SystemResponse.serverError([error]));
            });
        } else {
            const error = new BadFormatIdError(
              'id',
              id,
              `Application id [${id}] is not in the right format`,
            );
            resolve(SystemResponse.badRequestError([error]));
        }
      },
    );

    return promise;
  }

  public list({ query: {limit = 50, skip = 0} }: { query: IList }): Promise<IApplication[]> {

    logger.debug('ApplicationMiddleware - List');
    logger.info(`Getting [${limit}] applications (skipping [${skip}])...`);
    const promise: Promise<IApplication[]> = new Promise((resolve: any, reject: any) => {
      ApplicationMiddleware.applicationController
        .list(limit, skip)
        .then((applications: IApplication[]) => {
          resolve(applications);
        })
        .catch((error: IError) => {
          resolve(SystemResponse.serverError([error]));
        });
      },
    );

    return promise;
  }

  public async create({ body: { name, caption } }: { body: ICreate }) {

    logger.debug('ApplicationMiddleware - Create');
    logger.info(`Creating application with name [${name}] and caption [${caption}]...`);

    const promise: Promise<IApplication> = new Promise((resolve: any, reject: any) => {
      ApplicationMiddleware.applicationController
        .create(name, caption)
        .then((application: IApplication) => {
          resolve(application);
        })
        .catch((error: IError) => {
          if (error.type === DuplicateError.ERROR_TYPE) {
            resolve(SystemResponse.badRequestError([error]));
          } else {
            resolve(SystemResponse.serverError([error]));
          }
        });
    });

    return promise;
  }

  public async update({ body: { id, name, caption } }: { body: IUpdate }) {

    logger.debug('ApplicationMiddleware - Update');
    logger.info(`Updating application [${id}] with name [${name}] and caption [${caption}]...`);

    const promise: Promise<IApplication> = new Promise((resolve: any, reject: any) => {

      const idIsValid = BaseRepository.checkId(id);

      if (idIsValid) {

        ApplicationMiddleware.applicationController
          .update(id, name, caption)
          .then((application: IApplication) => {
            resolve(application);
          })
          .catch((error: IError) => {
            if (error.type === DuplicateError.ERROR_TYPE) {
              resolve(SystemResponse.badRequestError([error]));
            } else {
              resolve(SystemResponse.serverError([error]));
            }
          });
      } else {
          const error = new BadFormatIdError(
            'id',
            id,
            `Application id [${id}] is not in the right format`,
          );
          resolve(SystemResponse.badRequestError([error]));
      }
    });

    return promise;
  }
}
